package com.uttorok.pandaplaza;

/**
 * Class that realizes line indentation to make output more readable
 */
public class Indentation {
    /**
     * variable that contains the current indentation depth
     */
    private static int depth = 0;

    /**
     * Decrease indentation depth
     */
    public static void increaseDepth() {
        depth++;
    }

    /**
     * Increase indentation depth
     */
    public static void decreaseDepth() {
        depth--;
    }

    /**
     * @param value set indentation depth to this value
     */
    public static void setDepth(int value) {
        depth = value;
    }

    /**
     * @return      the current indentation depth
     */
    public static int getDepth() {
        return depth;
    }

    /**
     * @return      a string that contains as many tabulators as the depth
     */
    public static String indent() {
        String s = "";
        for (int i = 0; i < depth; i++) {
            s += "\t";
        }
        return s;
    }
}
