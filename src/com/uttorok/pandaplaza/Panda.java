package com.uttorok.pandaplaza;

import javax.swing.*;

/**
 * An abstract class that implements generic Panda behavior
 */
public abstract class Panda extends Animal {
    /**
     * Makes free pandas move
     */
    @Override
    public void act() {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": act()");

        if (animalAtFront == null) {
            tile.moveToNeighborTile(this);
        }

        Indentation.decreaseDepth();
    }

    /**
     * Makes the Panda die, frees pointers etc
     */
    @Override
    public void die(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");

        if(animalAtFront != null) {
            animalAtFront.releaseBackwards();
        }

        if(animalBehind != null) {
            this.releaseBackwards();
        }

        this.tile.animal = null;
        this.tile=null;

        Game.getInstance().getCurrentMap().removeAnimal(this);

        Indentation.decreaseDepth();
    }
}
