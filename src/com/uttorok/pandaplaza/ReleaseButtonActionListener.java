package com.uttorok.pandaplaza;

import java.awt.event.*;

/**
 * Action Listener for the button that lets the player release Pandas caught.
 */
public class ReleaseButtonActionListener implements ActionListener {

    /**
     * @param ae the event that invokes the action
     */
    public void actionPerformed(ActionEvent ae) {
        Orangutan ActiveOrangutan = Game.getInstance().getActiveOragutan();
        ActiveOrangutan.announceRelease();
        Game.getInstance().getCurrentMap().disableButtons();
        ActiveOrangutan.getTile().enableNeighborButtons();
    }
}
