package com.uttorok.pandaplaza;

import java.util.ArrayList;

/**
 * Absztrakt ősosztály
 */
public abstract class Thing implements IAction {
    protected Tile tile;

    public void setTile(Tile t) {
        tile = t;
    }

    public Tile getTile() {
        return tile;
    }

    // a következő függvények a szerializálás miatt kellenek itt
    public void setTiredPanda(TiredPanda tiredPanda) {}

    public void setWardrobes(ArrayList<Wardrobe> wardrobes) {}
}
