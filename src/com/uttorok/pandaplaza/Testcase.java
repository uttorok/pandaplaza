package com.uttorok.pandaplaza;

/**
 * Teszteset kezelésére szolgáló "szerializáló" osztály. A State kiterjesztése, az indító parancs kezelése az, amivel többet tud annál.
 */
public class Testcase extends State {

    /**
     * Az indító parancs
     */
    private Instruction instruction;

    public Instruction getInstruction() {
        return instruction;
    }

    public void setInstruction(Instruction instruction) {
        this.instruction = instruction;
    }

    /**
     * indító parancs nélküli konstruktor
     * @param size pálya mérete
     */
    public Testcase(int size) {
        super(size);
    }

    /**
     * @param size pálya mérete
     * @param instruction indító parancs
     */
    public Testcase(int size, Instruction instruction) {
        super(size);
        this.instruction = instruction;
    }

    /**
     * Indító parancs végrehajtása a betöltött teszteseten
     */
    public void executeTestcase() {
        this.getInstruction().execute();
    }
}
