package com.uttorok.pandaplaza;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Actin Listener for the top menu
 */
public class MenuItemActionListener implements ActionListener {
    private Map m;

    public MenuItemActionListener(Map mm) {
        m = mm;
    }

    /**
     * @param ae the event that invoked the action
     */
    public void actionPerformed(ActionEvent ae) {
        switch (ae.getActionCommand()) {
            case "newgame":
                break;
            case "exit":
                System.exit(0);
                break;
            case "about":
                String str = "Pandaplaza\n\n" +
                             "Created by Marcell Lévai,\n" +
                    "                    Kristóf Borbíró,\n" +
                    "                    Richárd Jakab,\n" +
                    "                    Martin Csépányi,\n" +
                    "                    Márk Ángyán\n\n" +
                             "Szoftver projekt laboratórium, 2019.";

                //image transform and resize
                ImageIcon imageIcon = new ImageIcon("source\\images\\icon\\panda_icon.png");
                Image image = imageIcon.getImage();
                Image newimg = image.getScaledInstance(44, 41,  java.awt.Image.SCALE_SMOOTH);
                imageIcon = new ImageIcon(newimg);

                JOptionPane.showMessageDialog(m.getFrame(), str,
                        "Information", JOptionPane.INFORMATION_MESSAGE,
                        imageIcon);
                break;
            default:
                break;
        }
    }
}
