package com.uttorok.pandaplaza;

public class WeakTile extends Tile {
    private int life;

    public WeakTile(){
        life = 20;
    }

    public void setLife(int l) {
        life = l;
    }

    public int getLife(){ return life; }

    public void setPanda(JumpyPanda jp){
        animal = jp;
    }

    @Override
    public int weakLife(){return life;}

    /**
     * Decrease weak tile life
     */
    public void decreaseLife(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": decreaseLife()" );
        if(life>1){
            life--;
        }
        else if(life==1) {
            life--;
            breakTile();
            animal.die();
        }
        else if(life==0) {
            animal.die();
        }

        Indentation.decreaseDepth();

    }

    /**
     * Take animals from old tile, and decrease life
     * @param a Animal which is took.
     */
    @Override
    public void take(Animal a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": take(" + a.getClass().getSimpleName() + ")");

        animal = a;
        a.tile = this;

        this.decreaseLife();

        Indentation.decreaseDepth();

    }

    /**
     * Tile is broken
     */
    public void breakTile(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": breakTile()" );
        Indentation.decreaseDepth();
    }

    /**
     * Animal jumped on to the tile which is decreased life
     * @param a Animal which is jumped.
     */
    @Override
    public void jumpedOntoBy(Animal a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": jumpedOntoBy(" + a.getClass().getSimpleName() + ")");
        decreaseLife();
        Indentation.decreaseDepth();
    }
}
