package com.uttorok.pandaplaza;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Action Listener for tile clicks
 */
public class TileButtonActionListener implements ActionListener {
    private Tile t;

    public TileButtonActionListener(Tile tt) {
        t = tt;
    }

    /**
     * This method handles Orangutan moving, turns, and everything things act
     * @param ae the Event that invoked the action
     */
    public void actionPerformed(ActionEvent ae) {

        // DEBUG STUFF
        System.out.print(t.getClass().getSimpleName());
        if(t.animal != null){
            System.out.print(" - " + t.getAnimal().getClass().getSimpleName());
        }
        if(t.getThing() != null) {
            System.out.print(" - " + t.getThing().getClass().getSimpleName());
        }
        System.out.print(" - Neighbors: " + t.getNeighbors().size());
        System.out.println();


        // REAL STUFF
        // Orangutan moves
        Game.getInstance().getActiveOragutan().moveTo(t);
        Game.getInstance().getCurrentMap().disableButtons();

        // animals and things act
        Game.getInstance().getCurrentMap().makeAct();

        // next turn
        Game.getInstance().switchActiveOrangutan();
        Game.getInstance().nextTurn();
    }
}
