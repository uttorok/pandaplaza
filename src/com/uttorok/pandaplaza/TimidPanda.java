package com.uttorok.pandaplaza;

import javax.swing.*;

/**
 * Class that implements behavior that is specific to Pandas that get scared
 */
public class TimidPanda extends Panda {
    public TimidPanda(){

    }

    /**
     * the TimidPanda gets scared and releases the Panda behind it, if any
     */
    private void getScared(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": getScared()");

        if(animalAtFront != null) {
            animalAtFront.releaseBackwards();
        }

        Indentation.decreaseDepth();
    }

    /**
     * @param s SlotMachine to react to
     */
    @Override
    public void reactionTo(SlotMachine s){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": reactionTo(" + s.getClass().getSimpleName() + ")");

        this.getScared();

        Indentation.decreaseDepth();
    }

    /**
     * @param screenScale helps scale the graphics properly
     */
    @Override
    public void draw(double screenScale) {
        try {
            if (tile != null)
                tile.setButtonIMG(new ImageIcon("source\\images\\animals\\timidpanda.png").getImage().getScaledInstance((int) (85 * screenScale), (int) (85 * screenScale), 100));

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
