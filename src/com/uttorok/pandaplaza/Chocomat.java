package com.uttorok.pandaplaza;

import java.util.Random;

public class Chocomat extends Thing {
    public Chocomat(){

    }

    /**
     * Chocomat acts (whistles)
     */
    @Override
    public void act(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": act()");

        Random r = new Random();
        int random = r.nextInt(100);
        if(random <= 30) {
            whistle();
        }

        Indentation.decreaseDepth();
    }

    /**
     * Chocomat whistles
     */
    public void whistle(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": whistle()");

        tile.affectNeighbors(this);

        Indentation.decreaseDepth();
    }
}
