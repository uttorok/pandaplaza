package com.uttorok.pandaplaza;

public class Exit extends Tile {
    public Exit(){
    }

    /**
     * @param o Orangutan leaves the Exit
     */
    public void remove(Orangutan o){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": remove(" + o.getClass().getSimpleName() + ")");

        animal = null;

        Indentation.decreaseDepth();

    }
}
