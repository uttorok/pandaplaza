package com.uttorok.pandaplaza;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Wardrobe extends Thing {

    private ArrayList<Wardrobe> wardrobes;

    public Wardrobe() {
        wardrobes = new ArrayList<Wardrobe>();
    }

    public void addOtherWardrobe(Wardrobe w) {
        wardrobes.add(w);
    }

    public ArrayList<Wardrobe> getWardrobes() {
        return wardrobes;
    }

    @Override
    public void setWardrobes(ArrayList<Wardrobe> wardrobes) {
        this.wardrobes = wardrobes;
    }

    /**
     * Act of Wardrobe
     */
    @Override
    public void act() {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": act()");

        tile.affect(this);

        Indentation.decreaseDepth();
    }

    /**
     * Teleporting Orangutan to another Wardrobe
     *
     * @param o Armchair only takes Orangutan first
     */
    public void teleport(Orangutan o) {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": teleport()");

        Tile freeWardrobeTile = getRandomFreeWardrobeTile();
        freeWardrobeTile.accept(o, tile);

        Indentation.decreaseDepth();
    }

    /**
     * Generating another Wardrobe's Tile
     */
    private Tile getRandomFreeWardrobeTile() {

        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": getRandomFreeWardrobeTile()");
        Indentation.decreaseDepth();
        Random rand = new Random();

        // a szekrény a teljes szekrénylistát tartalmazza, magát is beleértve
        // ezzel kizárjuk, hogy magára hivatkozzon
        int randomNum = rand.nextInt(wardrobes.size());
        while(wardrobes.get(randomNum) == this) {
            randomNum = rand.nextInt(wardrobes.size());
        }
        System.out.println("Szekrény száma a listában: " + randomNum);
        return wardrobes.get(randomNum).getTile();
    }
}
