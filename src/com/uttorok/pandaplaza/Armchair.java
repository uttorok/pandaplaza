package com.uttorok.pandaplaza;

public class Armchair extends Thing {
    private boolean occupied;

    private TiredPanda tiredPanda;

    public Armchair(){
        occupied = false;
        tiredPanda = null;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public TiredPanda getTiredPanda() {
        return tiredPanda;
    }

    @Override
    public void setTiredPanda(TiredPanda tiredPanda) {
        this.tiredPanda = tiredPanda;
    }

    /**
     * Armchair acts
     */
    @Override
    public void act(){
        if(!occupied) initiateSleep();
    }

    /**
     * Armchair takes Panda
     * @param p Panda sits into Armchair, t is the Panda's old Tile
     */
    public void takePanda(Panda p, Tile t){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": takePanda(" + p.getClass().getSimpleName() + ", " + t.getClass().getSimpleName() + ")");

        tile.accept(p, t);
        occupied=true;
        //tiredPanda=(TiredPanda)p;

        Indentation.decreaseDepth();
    }

    /**
     * Armchair releases Panda
     */
    public void releasePanda(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": releasePanda()");

        tiredPanda=null;
        occupied=false;

        Indentation.decreaseDepth();
    }

    /**
     * Affects Animals on neighbor tiles, only tiredPanda reacts by falling asleep in the Armchair
     */
    public void initiateSleep(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": initiateSleep()");

        tile.affectNeighbors(this);

        Indentation.decreaseDepth();
    }
}
