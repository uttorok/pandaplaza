package com.uttorok.pandaplaza;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        // Set look and Feel
        try{ UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) { }

        Game.getInstance().setCurrentMap(new Map());

        Game.getInstance().startGame();
        Game.getInstance().nextTurn();
    }
}
