package com.uttorok.pandaplaza;

import javax.swing.*;

/**
 * Orangutan class
 */
public class Orangutan extends Animal {
    /**
     * The Game in which the Orangutan is
     */
    private Game game;

    /**
     * The number of remaining steps for the Orangutan
     */
    private int remainingSteps;

    /**
     * How many steps until the Orangutan can take another Orangutan's Pandas
     */
    private int waitTime;

    /**
     * Whether the Orangutan has already been teleported in the current round
     */
    private boolean teleported;

    /**
     * Identifier of the Orangutan
     */
    private String name;

    public Orangutan(){
        game = Game.getInstance();
        remainingSteps = 20;
        waitTime = 0;
        teleported = false;
    }

    public int getRemainingSteps() {
        return remainingSteps;
    }

    public void setRemainingSteps(int remainingSteps) {
        this.remainingSteps = remainingSteps;
    }

    @Override
    public int getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Orangutan moves to new Tile
     *
     * @param t Tile where the Animal goes to
     */
    @Override
    public void moveTo(Tile t){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + t.getClass().getSimpleName() + ")");

        if(remainingSteps <= 0) {
            this.die();
        }
        else {
            Tile oldTile = tile;
            teleported = false;

            t.accept(this, tile);


            if(this.animalBehind != null){
                this.animalBehind.pullTo(oldTile);
            }

            if(waitTime>0) {
                waitTime--;
            }

            this.decreaseSteps();
        }

        Indentation.decreaseDepth();
    }

    /**
     * Orangutan goes out on Exit, kill all Pandas behind
     */
    public void killPandas(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");

        if(animalBehind != null){
            animalBehind.recursivelyKill(this);
        }

        Indentation.decreaseDepth();
    }

    /**
     * Orangutan dies
     */
    @Override
    public void die(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");
        //TODO Ezt a játék osztályban kéne megvalósítani vagy paraméter kéne
        releaseBackwards();
        this.tile = null;
        Game.getInstance().getCurrentMap().removeAnimal(this);
        Game.getInstance().removeOrangutan(this);
        Indentation.decreaseDepth();
    }

    /**
     * Orangutan reacts to Wardrobe (steps on a Tile with Wardrobe)
     *
     * @param w Wardrobe to react to
     */
    @Override
    public void reactionTo(Wardrobe w){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + w.getClass().getSimpleName() + ")");

        Tile old = tile;
        if(!teleported) {
            w.teleport(this);
            teleported = true;
            if(animalBehind != null) {
                animalBehind.pullTo(old);
            }
        }

        Indentation.decreaseDepth();
    }

    /**
     * Orangutan reacts to Panda (steps on a Tile with Panda om it)
     *
     * @param p Panda to react to
     */
    public void reactionTo(Panda p){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + p.getClass().getSimpleName() + ")");

        p.chain(this, animalBehind);
        //game.addPoints(1, this);

        Indentation.decreaseDepth();
    }

    /**
     * This method gets called when two Orangutans meet and it handles Panda switching
     * @param o the Orangutan that it bumps into
     */
    public void reactionTo(Orangutan o) {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + o.getClass().getSimpleName() + ")");
        Animal temp = o.animalBehind;
        o.setAnimalBehind(this.animalBehind);
        this.setAnimalBehind(temp);
        //this.takePanda((Panda) o.animalBehind);

        if(this.animalBehind != null) {
            this.animalBehind.setAnimalAtFront(this);
        }
        if(o.animalBehind != null) {
            o.animalBehind.setAnimalAtFront(o);
        }

        // a MoveTo a végén automatikusan levon egyet, ezért HÁK
        this.waitTime = 3 + 1;
        o.setWaitTime(3);
        Indentation.decreaseDepth();
    }

    /**
     * Decreases remaining steps
     */
    public void decreaseSteps(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");

        if(--remainingSteps == 0){
            die();
        }

        Indentation.decreaseDepth();
    }

    @Override
    public int getLife() { return remainingSteps; }


    /*public void switchPanda(Orangutan o){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + p.getClass().getSimpleName() + ", " + o.getClass().getSimpleName() + ")");
        Animal temp =o.animalBehind;
        o.animalBehind=this.animalBehind;
        this.animalBehind=temp;
        //this.takePanda((Panda) o.animalBehind);
        Indentation.decreaseDepth();
    }*/
    //félre lett értelmezve, ez kuka      -> nem lett félreértelmezve ಠ_ಠ
    /*public void takePanda(Panda p){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "( "+ p.getClass().getSimpleName() + ")");
        Animal seged=p;
        while(seged.animalBehind!=null) seged = seged.animalBehind;//Nem chainel lett megoldva, ez elvileg beemeli az egész láncot előre
        p.animalAtFront=this;
        seged.animalBehind=this.animalBehind;
        this.animalBehind.animalAtFront=seged.animalBehind;
        this.animalBehind=p;
        p.animalBehind.setFirstPosition(p);
        Indentation.decreaseDepth();
    }*/


    @Override
    public String toString() {
        return "Orangutan";
    }

    /**
     * @param screenScale helps scale the graphics properly
     */
    @Override
    public void draw(double screenScale) {
        try {
            if (tile != null) {
                if (this.getName().equals("Harambe"))
                    tile.setButtonIMG(new ImageIcon("source\\images\\animals\\orangutan1.png").getImage().getScaledInstance((int) (85 * screenScale), (int) (85 * screenScale), 100));
                else
                    tile.setButtonIMG(new ImageIcon("source\\images\\animals\\orangutan2.png").getImage().getScaledInstance((int) (85 * screenScale), (int) (85 * screenScale), 100));
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}


