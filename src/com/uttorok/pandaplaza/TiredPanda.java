package com.uttorok.pandaplaza;

import javax.swing.*;

/**
 * Implementation of the behavior specific to tha Panda that can sit down to Armchairs
 */
public class TiredPanda extends Panda {
    private Armchair armchair;
    private static final int maxEnergy = 5;
    private static final int minEnergy = 0;
    private int energy;

    public TiredPanda(){
        energy = maxEnergy;
        armchair = null;

    }

    @Override
    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int e){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": setEnergy(" + "int" + ")");
        energy = e;
        Indentation.decreaseDepth();
    }

    @Override
    public Armchair getArmchair() {
        return armchair;
    }

    public void setArmchair(Armchair armchair) {
        this.armchair = armchair;
    }

    /**
     * @param a Armchair to sit on
     */
    private void sit(Armchair a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": sit(" + a.getClass().getSimpleName() + ")");

        Tile old_tile = tile;
        a.takePanda(this, tile);
        if(tile != old_tile) {
            armchair = a;
            if(animalAtFront != null) {
                animalAtFront.releaseBackwards();
            }
        }

        Indentation.decreaseDepth();
    }

    /**
     * @param a Armchair that the TiredPanda will sit on, if tired
     */
    @Override
    public void reactionTo(Armchair a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": reactionTo(" + a.getClass().getSimpleName() + ")");

        if(energy == 0) {
            sit(a);
        }

        Indentation.decreaseDepth();
    }

    /**
     * Makes the TiredPanda move (and lose energy) if free,
     * otherwise if it's in an armchair, it will increase it's energy and stand up if it's at maximum energy
     */
    @Override
    public void act(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": act()");

        if (animalAtFront == null) {
            if(armchair != null) {
                if(energy == maxEnergy) {
                    this.stand();
                }
                else {
                    increaseEnergy();
                }
            }
            else {
                decreaseEnergy();
                if (animalAtFront == null) {
                    tile.moveToNeighborTile(this);
                }
            }
        }

        Indentation.decreaseDepth();
    }

    /**
     * Increse the TiredPanda's energy until max is reached
     */
    private void increaseEnergy(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": increaseEnergy()");

        if(energy < maxEnergy) {
            energy++;
        }

        Indentation.decreaseDepth();
    }

    /**
     * Decrease the TiredPanda's energy, until min is reached
     */
    private void decreaseEnergy(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": decreaseEnergy()");

        if(energy > minEnergy) {
            energy--;
        }

        Indentation.decreaseDepth();
    }

    /**
     * TiredPanda stands up from the armchair
     */
    private void stand(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": stand()");

        armchair.releasePanda();
        armchair = null;
        tile.moveToNeighborTile(this);

        Indentation.decreaseDepth();
    }

    /**
     * @param t Tile move Panda to this tile
     */
    @Override
    public void pullTo(Tile t){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": pullTo(" + t.getClass().getSimpleName() + ")");

        this.decreaseEnergy();
        Tile old_tile =  tile;
        t.accept(this, tile);

        if(animalBehind != null) {
            animalBehind.pullTo(old_tile);
        }

        Indentation.decreaseDepth();
    }

    /**
     * @param screenScale helps scale the graphics properly
     */
    @Override
    public void draw(double screenScale) {
        try {
            if (tile != null)
                tile.setButtonIMG(new ImageIcon("source\\images\\animals\\tiredpanda.png").getImage().getScaledInstance((int) (85 * screenScale), (int) (85 * screenScale), 100));

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
