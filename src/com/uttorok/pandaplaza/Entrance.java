package com.uttorok.pandaplaza;

/**
 * The tile that serves as an Entrance to the Mall
 */
public class Entrance extends Tile {
    public Entrance(){
    }

    /**
     * This method gets called in case an Orangutan steps onto the Entrance from the Exit
     * @param o the Orangutan that moves to the entrance
     * @param e the orangutan came from the exit
     */
    /*@Override
    public void accept(Orangutan o, Exit e){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": accept(" + o.getClass().getSimpleName() + ", " + e.getClass().getSimpleName() + ")");
        e.remove(o);
        this.take(o);
        o.killPandas();
        Indentation.decreaseDepth();

    }*/
}
