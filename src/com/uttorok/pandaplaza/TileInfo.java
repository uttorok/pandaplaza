package com.uttorok.pandaplaza;

/**
 * Csempék és rajtuk lévő dolgok információinak tárolására szolgáló, szerializáló osztály
 */
public class TileInfo {
    /**
     * Az adott csempe sorszáma
     */
    private int tileNumber;

    /**
     * Az adott csempe típusa (NormalTile, WeakTile)
     */
    private String tileType;

    /**
     * Csempe paramétere (normal esetén 0, egyébként a WeakTile élete)
     */
    private int tileParameter;

    /**
     * Thing a csempén (None, SlotMachine, ChocoMat, Wardrobe, Armchair)
     */
    private String thingOnTile;

    /**
     * Állat a csempén (JumpyPanda, TiredPanda, TimidPanda, Orangutan1, Orangutan2, None)
     */
    private String animalOnTile;

    /**
     * Állat paramétere (Orangután esetén a maradék lépése száma, TiredPanda esetén az energia)
     */
    private int animalParameter;

    /**
     * A sorban előtte lévő állat csempéjének száma. Ha nincs, 0.
     */
    private int animalFrontTileNum;

    /**
     * A sorban mögötte lévő állat csempéjének száma. Ha nincs, 0.
     */
    private int animalBehindTileNum;

    /**
     * Orangutánnak mennyit kell várni, hogy elvehessen Pandát másik Orangutántól (Orangután esetén 0-3, egyébként 0)
     */
    private int orangutanWaitTime;

    public int getTileNumber() {
        return tileNumber;
    }

    public void setTileNumber(int tileNumber) {
        this.tileNumber = tileNumber;
    }

    public String getTileType() {
        return tileType;
    }

    public void setTileType(String tileType) {
        this.tileType = tileType;
    }

    public int getTileParameter() {
        return tileParameter;
    }

    public void setTileParameter(int tileParameter) {
        this.tileParameter = tileParameter;
    }

    public String getThingOnTile() {
        return thingOnTile;
    }

    public void setThingOnTile(String thingOnTile) {
        this.thingOnTile = thingOnTile;
    }

    public String getAnimalOnTile() {
        return animalOnTile;
    }

    public void setAnimalOnTile(String animalOnTile) {
        this.animalOnTile = animalOnTile;
    }

    public int getAnimalParameter() {
        return animalParameter;
    }

    public void setAnimalParameter(int animalParameter) {
        this.animalParameter = animalParameter;
    }

    public int getAnimalFrontTileNum() {
        return animalFrontTileNum;
    }

    public void setAnimalFrontTileNum(int animalFrontTileNum) {
        this.animalFrontTileNum = animalFrontTileNum;
    }

    public int getAnimalBehindTileNum() {
        return animalBehindTileNum;
    }

    public void setAnimalBehindTileNum(int animalBehindTileNum) {
        this.animalBehindTileNum = animalBehindTileNum;
    }

    public int getOrangutanWaitTime() {
        return orangutanWaitTime;
    }

    public void setOrangutanWaitTime(int orangutanWaitTime) {
        this.orangutanWaitTime = orangutanWaitTime;
    }

    /**
     * Üres konstruktor
     */
    public TileInfo() {

    }

    /**
     * Felparaméterezett konstruktor
     * @param tileNumber Az adott csempe sorszáma
     * @param tileType Az adott csempe típusa (NormalTile, WeakTile)
     * @param tileParameter Csempe paramétere (normal esetén 0, egyébként a WeakTile élete)
     * @param thingOnTile Thing a csempén (None, SlotMachine, ChocoMat, Wardrobe, Armchair)
     * @param animalOnTile Állat a csempén (JumpyPanda, TiredPanda, TimidPanda, Orangutan1, Orangutan2, None)
     * @param animalParameter Állat paramétere (Orangután esetén a maradék lépése száma, TiredPanda esetén az energia)
     * @param animalFrontTileNum  A sorban előtte lévő állat csempéjének száma. Ha nincs, 0.
     * @param animalBehindTileNum A sorban mögötte lévő állat csempéjének száma. Ha nincs, 0.
     * @param orangutanWaitTime Orangutánnak mennyit kell várni, hogy elvehessen Pandát másik Orangutántól (Orangután esetén 0-3, egyébként 0)
     */
    public TileInfo(int tileNumber, String tileType, int tileParameter, String thingOnTile, String animalOnTile, int animalParameter, int animalFrontTileNum, int animalBehindTileNum, int orangutanWaitTime) {
        this.tileNumber = tileNumber;
        this.tileType = tileType;
        this.tileParameter = tileParameter;
        this.thingOnTile = thingOnTile;
        this.animalOnTile = animalOnTile;
        this.animalParameter = animalParameter;
        this.animalFrontTileNum = animalFrontTileNum;
        this.animalBehindTileNum = animalBehindTileNum;
        this.orangutanWaitTime = orangutanWaitTime;
    }
}
