package com.uttorok.pandaplaza;

import javax.print.attribute.standard.NumberOfDocuments;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * UNUSED, DEPRECATED CODE
 */
public class SkeletonTest {
    public SkeletonTest(){}

    public void runTest(){
        Scanner in = new Scanner(System.in);

        System.out.println("Melyik teszt fusson?\n");
        System.out.println("O - Orangután lép");
        System.out.println("P - Szabad panda lép");
        System.out.println("D - Szekrény kifejti hatását");
        System.out.println("A - Fotel kifejti hatását");
        System.out.println("S - Játékgép sípol");
        System.out.println("C - Csokiautomata csilingel");
        System.out.println("Q - TiredPanda megpróbál felállni a fotelből\n");

        System.out.println("K - Kilépés\n");

        String answer = "-";

        while(!answer.equals("O") && !answer.equals("P") && !answer.equals("D") && !answer.equals("A") && !answer.equals("S") && !answer.equals("C") && !answer.equals("Q") && !answer.equals("K")){
            answer = in.nextLine().substring(0, 1).toUpperCase();
        }

        switch(answer){
            case "O" :
                orangutanLep();
                //runTest();
                break;
            case "P" :
                szabadPandaLep();
                //runTest();
                break;
            case "D" :
                szekrenyKifejtiHatasat();
                //runTest();
                break;
            case "A" :
                fotelKifejtiHatasat();
                //runTest();
                break;
            case "S" :
                jatekgepSipol();
                //runTest();
                break;
            case "C" :
                csokiAutomataCsilingel();
                //runTest();
                break;
            case "Q" :
                tiredPandaMegprobalFelallniAFotelbol();
                //runTest();
                break;
            case "K" :
                break;
            default :
                System.out.println("Baj van");
                break;
        }
    }

    public void orangutanLep() {
        System.out.println("O - Orangután lép");
        //Deklarációk
        Scanner in = new Scanner(System.in);
        Orangutan o = new Orangutan();
        ArrayList<Panda> pandas = new ArrayList<>();
        ArrayList<Tile> tiles = new ArrayList<>();


        System.out.println("Hány lépés van még hátra?\n");
        int steps = in.nextInt();

        //Ha több mint egy lépése van az orángutánnak
        if (steps > 0) {
            int weaklife = -1;
            String pandabehind = new String();
            String otile = new String();
            String onext = new String();

            System.out.println("Milyen csempén van? (W-weak tile/R-normal/E-entrance/X-exit)\n");


            while (!otile.equals("R") && !otile.equals("W") && !otile.equals("E") && !otile.equals("X")) {
                otile = in.nextLine().toUpperCase();
            }
            //Ha a panda gyenge csempén van
            // TODO orángután csempéjének példányosítása
            if (otile.equals("W")) {
                System.out.println("Hány élete van a gyenge csempének?(szám)\n");
                while (weaklife < 1) {
                    weaklife = in.nextInt();
                }

            }
            //Az orángután mögött lévő pandák sora

            do {
                pandabehind = "ASD";
                System.out.println("Van mögötte Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                while (!pandabehind.equals("Z") && !pandabehind.equals("J") && !pandabehind.equals("T") && !pandabehind.equals("N")) {
                    pandabehind = in.nextLine().toUpperCase();
                }
                switch (pandabehind) {
                    //Milyen panda áll mögötte--> pandák szintje
                    case "Z":
                        int energy = -1;
                        String ptile = new String();
                        TiredPanda tp = new TiredPanda();
                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                        while (energy <= 0) {
                            energy = in.nextInt();
                        }
                        tp.setEnergy(energy);
                        pandas.add(tp);
                        //Az adott panda milyen csempén áll-->panda csempéjének a szintje
                        System.out.println("Milyen csempén van a Panda? (W-weak tile/R-normal tile)");
                        while (!ptile.equals("W") && !ptile.equals("R")) {
                            ptile = in.nextLine().toUpperCase();
                        }
                        if (ptile.equals("W")) {
                            int life = -1;
                            System.out.println("Hány élete van a gyenge csempének?(szám)\n");
                            while (life < 1) {
                                life = in.nextInt();
                            }
                            WeakTile wt = new WeakTile();
                            wt.setLife(life);
                            tiles.add(wt);
                        } else {
                            Tile t = new Tile();
                            tiles.add(t);
                        }

                        break;
                    case "J":
                        JumpyPanda jp = new JumpyPanda();
                        String ptile1 = new String();
                        pandas.add(jp);
                        //Az adott panda milyen csempén áll-->panda csempéjének a szintje
                        System.out.println("Milyen csempén van a Panda? (W-weak tile/R-normal tile)");
                        while (!ptile1.equals("W") && !ptile1.equals("R")) {
                            ptile1 = in.nextLine().toUpperCase();
                        }
                        if (ptile1.equals("W")) {
                            int life = -1;
                            System.out.println("Hány élete van a gyenge csempének?(szám)\n");
                            while (life < 1) {
                                life = in.nextInt();
                            }
                            WeakTile wt = new WeakTile();
                            wt.setLife(life);
                            tiles.add(wt);
                        } else {
                            Tile t = new Tile();
                            tiles.add(t);
                        }
                        break;
                    case "T":
                        TimidPanda tim = new TimidPanda();
                        String ptile2 = new String();
                        pandas.add(tim);
                        //Az adott panda milyen csempén áll-->panda csempéjének a szintje
                        System.out.println("Milyen csempén van a Panda? (W-weak tile/R-normal tile)");
                        while (!ptile2.equals("W") && !ptile2.equals("R")) {
                            ptile2 = in.nextLine().toUpperCase();
                        }
                        if (ptile2.equals("W")) {
                            int life = -1;
                            System.out.println("Hány élete van a gyenge csempének?(szám)\n");
                            while (life < 1) {
                                life = in.nextInt();
                            }
                            WeakTile wt = new WeakTile();
                            wt.setLife(life);
                            tiles.add(wt);
                        } else {
                            Tile t = new Tile();
                            tiles.add(t);
                        }
                        break;
                    case "N":
                        break;
                }

            }
            while (!pandabehind.equals("N"));
            //Pandák felfűzése láncba
           if (pandas.size()>3) {
                pandas.get(0).animalAtFront = o;
                pandas.get(0).setFirstPosition(o);
                pandas.get(0).animalBehind = pandas.get(1);
                pandas.get(pandas.size() - 1).animalAtFront = pandas.get(pandas.size() - 2);
                pandas.get(pandas.size() - 1).animalBehind = null;
                for (int i = 1; i < pandas.size() - 2; i++) {
                    pandas.get(i).animalAtFront = pandas.get(i - 1);
                    pandas.get(i).animalBehind = pandas.get(i + 1);
                }
            }
            else{
                switch(pandas.size()){
                    case 1 :
                        pandas.get(0).animalAtFront = o;
                        pandas.get(0).animalBehind = null;
                        break;
                    case 2 :
                        pandas.get(0).animalAtFront = o;
                        pandas.get(0).animalBehind = pandas.get(1);
                        pandas.get(1).animalAtFront = pandas.get(0);
                        pandas.get(1).animalBehind = null;
                        break;
                    case 3 :
                        pandas.get(0).animalAtFront = o;
                        pandas.get(0).animalBehind = pandas.get(1);
                        pandas.get(1).animalAtFront = pandas.get(0);
                        pandas.get(1).animalBehind = pandas.get(2);
                        pandas.get(2).animalAtFront = pandas.get(1);
                        pandas.get(2).animalBehind =null;
                        break;
                }
            }


            //Kész van a pandasor és a szomszédos mezők is ismerik egymást, most megtudjuk, hogy hova lép a panda

            System.out.println("Milyen csempére lép az Orángután?(W-weak tile/R-normal/E-entrance/X-exit)\n");

            while (!onext.equals("R") && !onext.equals("W") && !onext.equals("E") && !onext.equals("X")) {
                onext = in.nextLine().toUpperCase();
            }
            switch (onext) {
                case "W": //gyenge csempére szeretne lépni
                    int life = -1;
                    WeakTile w = new WeakTile();
                    System.out.println("Hány élete van a gyenge csempének?(szám)\n");
                    while (life < 0) {
                        life = in.nextInt();
                    }
                    w.setLife(life);
                    switch (otile) {
                        case "W"://gyenge csempéről
                            WeakTile wt = new WeakTile();
                            wt.setLife(weaklife);
                            o.setTile(wt);
                            if (w.getLife() == 0) {
                                o.die();
                            } else if (w.getLife() == 1) {
                                String pandaontile = new String();
                                System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                                while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                    pandaontile = in.nextLine().toUpperCase();
                                }
                                switch (pandaontile) {
                                    case "Z":
                                        int energy = -1;
                                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                        while (energy <= 0) {
                                            energy = in.nextInt();
                                        }
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik
                                        TiredPanda tp = new TiredPanda();
                                        tp.setEnergy(energy);
                                        tp.chain(o, pandas.get(0));
                                        o.moveTo(w);
                                        w.breakTile();

                                        break;
                                    case "J":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        JumpyPanda jp = new JumpyPanda();
                                        jp.chain(o, pandas.get(0));
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                    case "T":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        TimidPanda timp = new TimidPanda();
                                        timp.chain(o, pandas.get(0));
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                    case "N":
                                        //Ahova lépni szeretne, ott nincs panda
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                }
                            } else {
                                String pandaontile = new String();
                                System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                                while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                    pandaontile = in.nextLine().toUpperCase();
                                }
                                switch (pandaontile) {
                                    case "Z":
                                        int energy = -1;
                                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                        while (energy <= 0) {
                                            energy = in.nextInt();
                                        }
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik
                                        TiredPanda tp = new TiredPanda();
                                        tp.setEnergy(energy);
                                        tp.chain(o, pandas.get(0));
                                        w.decreaseLife();
                                        o.moveTo(w);
                                        break;
                                    case "J":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        JumpyPanda jp = new JumpyPanda();
                                        jp.chain(o, pandas.get(0));
                                        w.decreaseLife();
                                        o.moveTo(w);
                                        break;
                                    case "T":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        TimidPanda timp = new TimidPanda();
                                        timp.chain(o, pandas.get(0));
                                        o.moveTo(w);
                                        w.decreaseLife();
                                        break;
                                    case "N":
                                        //Ahova lépni szeretne, ott nincs panda
                                        o.moveTo(w);
                                        w.decreaseLife();
                                        break;
                                }
                            }
                            break;
                        case "R": //normál csempéről
                            Tile t1 = new Tile();
                            o.setTile(t1);
                            if (w.getLife() == 0) {
                                o.die();
                            } else if (w.getLife() == 1) {
                                String pandaontile = new String();
                                System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                                while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                    pandaontile = in.nextLine().toUpperCase();
                                }
                                switch (pandaontile) {
                                    case "Z":
                                        int energy = -1;
                                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                        while (energy <= 0) {
                                            energy = in.nextInt();
                                        }
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik
                                        TiredPanda tp = new TiredPanda();
                                        tp.setEnergy(energy);
                                        if (pandas.isEmpty()) {
                                            pandas.add(tp);
                                        } else {
                                            tp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();

                                        break;
                                    case "J":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        JumpyPanda jp = new JumpyPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(jp);
                                        } else {
                                            jp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                    case "T":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        TimidPanda timp = new TimidPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(timp);
                                        } else {
                                            timp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                    case "N":
                                        //Ahova lépni szeretne, ott nincs panda
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                }
                            } else {
                                String pandaontile = new String();
                                System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                                while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                    pandaontile = in.nextLine().toUpperCase();
                                }
                                switch (pandaontile) {
                                    case "Z":
                                        int energy = -1;
                                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                        while (energy <= 0) {
                                            energy = in.nextInt();
                                        }
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik
                                        TiredPanda tp = new TiredPanda();
                                        tp.setEnergy(energy);
                                        if (pandas.isEmpty()) {
                                            pandas.add(tp);
                                        } else {
                                            tp.chain(o, pandas.get(0));
                                        }
                                        w.decreaseLife();
                                        o.moveTo(w);
                                        break;
                                    case "J":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        JumpyPanda jp = new JumpyPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(jp);
                                        } else {
                                            jp.chain(o, pandas.get(0));
                                        }
                                        w.decreaseLife();
                                        o.moveTo(w);
                                        break;
                                    case "T":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        TimidPanda timp = new TimidPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(timp);
                                        } else {
                                            timp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.decreaseLife();
                                        break;
                                    case "N":
                                        //Ahova lépni szeretne, ott nincs panda
                                        o.moveTo(w);
                                        w.decreaseLife();
                                        break;
                                }
                            }
                            break;
                        case "E": //bejáratról
                            Entrance e = new Entrance();
                            o.setTile(e);
                            if (w.getLife() == 0) {
                                o.die();
                            } else if (w.getLife() == 1) {
                                String pandaontile = new String();
                                System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                                while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                    pandaontile = in.nextLine().toUpperCase();
                                }
                                switch (pandaontile) {
                                    case "Z":
                                        int energy = -1;
                                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                        while (energy <= 0) {
                                            energy = in.nextInt();
                                        }
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik
                                        TiredPanda tp = new TiredPanda();
                                        tp.setEnergy(energy);
                                        if (pandas.isEmpty()) {
                                            pandas.add(tp);
                                        } else {
                                            tp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();

                                        break;
                                    case "J":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        JumpyPanda jp = new JumpyPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(jp);
                                        } else {
                                            jp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                    case "T":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        TimidPanda timp = new TimidPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(timp);
                                        } else {
                                            timp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                    case "N":
                                        //Ahova lépni szeretne, ott nincs panda
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                }
                            } else {
                                String pandaontile = new String();
                                System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                                while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                    pandaontile = in.nextLine().toUpperCase();
                                }
                                switch (pandaontile) {
                                    case "Z":
                                        int energy = -1;
                                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                        while (energy <= 0) {
                                            energy = in.nextInt();
                                        }
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik
                                        TiredPanda tp = new TiredPanda();
                                        tp.setEnergy(energy);
                                        if (pandas.isEmpty()) {
                                            pandas.add(tp);
                                        } else {
                                            tp.chain(o, pandas.get(0));
                                        }
                                        w.decreaseLife();
                                        o.moveTo(w);
                                        break;
                                    case "J":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        JumpyPanda jp = new JumpyPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(jp);
                                        } else {
                                            jp.chain(o, pandas.get(0));
                                        }
                                        w.decreaseLife();
                                        o.moveTo(w);
                                        break;
                                    case "T":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        TimidPanda timp = new TimidPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(timp);
                                        } else {
                                            timp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.decreaseLife();
                                        break;
                                    case "N":
                                        //Ahova lépni szeretne, ott nincs panda
                                        o.moveTo(w);
                                        w.decreaseLife();
                                        break;
                                }
                            }
                            break;
                        case "X"://kijáratról
                            Exit x = new Exit();
                            o.setTile(x);
                            if (w.getLife() == 0) {
                                o.die();
                            } else if (w.getLife() == 1) {
                                String pandaontile = new String();
                                System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                                while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                    pandaontile = in.nextLine().toUpperCase();
                                }
                                switch (pandaontile) {
                                    case "Z":
                                        int energy = -1;
                                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                        while (energy <= 0) {
                                            energy = in.nextInt();
                                        }
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik
                                        TiredPanda tp = new TiredPanda();
                                        tp.setEnergy(energy);
                                        if (pandas.isEmpty()) {
                                            pandas.add(tp);
                                        } else {
                                            tp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();

                                        break;
                                    case "J":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        JumpyPanda jp = new JumpyPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(jp);
                                        } else {
                                            jp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                    case "T":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        TimidPanda timp = new TimidPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(timp);
                                        } else {
                                            timp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                    case "N":
                                        //Ahova lépni szeretne, ott nincs panda
                                        o.moveTo(w);
                                        w.breakTile();
                                        break;
                                }
                            } else {
                                String pandaontile = new String();
                                System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                                while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                    pandaontile = in.nextLine().toUpperCase();
                                }
                                switch (pandaontile) {
                                    case "Z":
                                        int energy = -1;
                                        System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                        while (energy <= 0) {
                                            energy = in.nextInt();
                                        }
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik
                                        TiredPanda tp = new TiredPanda();
                                        tp.setEnergy(energy);
                                        if (pandas.isEmpty()) {
                                            pandas.add(tp);
                                        } else {
                                            tp.chain(o, pandas.get(0));
                                        }
                                        w.decreaseLife();
                                        o.moveTo(w);
                                        break;
                                    case "J":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        JumpyPanda jp = new JumpyPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(jp);
                                        } else {
                                            jp.chain(o, pandas.get(0));
                                        }
                                        w.decreaseLife();
                                        o.moveTo(w);
                                        break;
                                    case "T":
                                        //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                        //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                        //A csempe eltörik //TODO ilyenkor a chain után is van pull to
                                        TimidPanda timp = new TimidPanda();
                                        if (pandas.isEmpty()) {
                                            pandas.add(timp);
                                        } else {
                                            timp.chain(o, pandas.get(0));
                                        }
                                        o.moveTo(w);
                                        w.decreaseLife();
                                        break;
                                    case "N":
                                        //Ahova lépni szeretne, ott nincs panda
                                        o.moveTo(w);
                                        w.decreaseLife();
                                        break;
                                }
                            }
                            break;
                    }
                    break;
                case "R": //Normál csempére szeretne lépni
                    Tile t = new Tile();
                    switch (otile) {
                        case "W"://gyenge csempéről
                            WeakTile wt = new WeakTile();
                            wt.setLife(weaklife);
                            o.setTile(wt);
                            String pandaontile = new String();
                            System.out.println("Van rajta Panda, ha igen milyen? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda/N-nincs)\n");
                            while (!pandaontile.equals("Z") && !pandaontile.equals("J") && !pandaontile.equals("T") && !pandaontile.equals("N")) {
                                pandaontile = in.nextLine().toUpperCase();
                            }
                            switch (pandaontile) {
                                case "Z":
                                    int energy = -1;
                                    System.out.println("Mennyi energiája van a TiredPandának?(szám) \n");
                                    while (energy <= 0) {
                                        energy = in.nextInt();
                                    }
                                    //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                    //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                    TiredPanda tp = new TiredPanda();
                                    tp.setEnergy(energy);
                                    if (pandas.isEmpty()) {
                                        pandas.add(tp);
                                    } else {
                                        tp.chain(o, pandas.get(0));
                                    }
                                    o.moveTo(t);
                                    break;
                                case "J":
                                    //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                    //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                    //TODO ilyenkor a chain után is van pull to
                                    JumpyPanda jp = new JumpyPanda();
                                    if (pandas.isEmpty()) {
                                        pandas.add(jp);
                                    } else {
                                        jp.chain(o, pandas.get(0));
                                    }
                                    o.moveTo(t);
                                    break;
                                case "T":
                                    //Ahova lépni szeretne, ott panda van, ezért a pandát elkapja
                                    //A panda hozzá lesz láncolva többi pandához és utána lép az orángután
                                    //TODO ilyenkor a chain után is van pull to
                                    TimidPanda timp = new TimidPanda();
                                    if (pandas.isEmpty()) {
                                        pandas.add(timp);
                                    } else {
                                        timp.chain(o, pandas.get(0));
                                    }
                                    o.moveTo(t);
                                    break;
                                case "N":
                                    //Ahova lépni szeretne, ott nincs panda
                                    o.moveTo(t);
                                    break;
                            }
                            break;
                        case "R"://normál csempéről
                            Tile t1 = new Tile();
                            o.setTile(t1);
                            o.moveTo(t);
                            break;
                        case "E"://bejáratról
                            Entrance e = new Entrance();
                            o.setTile(e);
                            o.moveTo(t);
                            break;
                        case "X"://kijáratról
                            System.out.println("Ez nem lehetséges");
                            break;
                    }
                    break;
                case "E": //Bejáratra szeretne lépni
                    Entrance en = new Entrance();
                    switch (otile) {
                        case "W"://gyenge csempéről
                            WeakTile wt = new WeakTile();
                            wt.setLife(weaklife);
                            o.setTile(wt);
                            o.moveTo(en);
                            break;
                        case "R"://normál csempéről
                            Tile t2 = new Tile();
                            o.setTile(t2);
                            o.moveTo(en);
                            break;
                        case "E"://bejáratról
                            System.out.println("Ez nem lehetséges");
                            break;
                        case "X"://kijáratról
                            Exit ex = new Exit();
                            o.setTile(ex);
                            o.moveTo(en);
                            o.killPandas();
                            break;
                    }
                    break;
                case "X": //Kijáratra szeretne lépni
                    Exit e = new Exit();
                    switch (otile) {
                        case "W"://gyenge csempéről
                            WeakTile wt = new WeakTile();
                            wt.setLife(weaklife);
                            o.setTile(wt);
                            o.moveTo(e);
                            break;
                        case "R"://normál csempéről
                            Tile t3 = new Tile();
                            o.setTile(t3);
                            o.moveTo(e);
                            break;
                        case "E"://bejáratról
                            System.out.println("Ez nem lehetséges");
                            break;
                        case "X"://kijáratról
                            System.out.println("Ez nem lehetséges");
                            break;
                    }
                    break;
            }

        }
        else{
            //Nincs több lépése a pandának, ilyenkor meghal
            o.die();
        }

    }

    public void szabadPandaLep() {
        System.out.println("P - Szabad panda lép");
        Scanner in = new Scanner(System.in);


        System.out.println("Milyen Panda? (Z-TiredPanda/J-JumpyPanda/T-TimidPanda)\n");

        String pandatype = new String();
        String tiletype = new String();

        while (!pandatype.equals("Z") && !pandatype.equals("T") && !pandatype.equals("J")) {
            pandatype = in.nextLine().substring(0, 1).toUpperCase();
        }

        if (pandatype.equals("T")) {

            System.out.println("Milyen csempén van a Panda? (R-normál tile/W-weak tile)\n");

            while (!tiletype.equals("W") && !tiletype.equals("R")) {
                tiletype = in.nextLine().substring(0, 1).toUpperCase();
            }

            if (tiletype.equals("R")) {
                TimidPanda tp = new TimidPanda();
                Tile t1 = new Tile();
                Tile t2 = new Tile();

                t1.addNeighbor(t2);
                t2.addNeighbor(t1);

                t1.take(tp);
                tp.tile = t1;

                t1.moveToNeighborTile(tp);

            } else if (tiletype.equals("W")) {

                System.out.println("Hány élete van a gyenge csempének?\n");
                int lifeOfTile = in.nextInt();

                TimidPanda tp = new TimidPanda();
                Tile t = new Tile();
                WeakTile wt = new WeakTile();

                t.addNeighbor(wt);
                wt.addNeighbor(t);
                tp.tile = t;
                t.take(tp);

                if (lifeOfTile == 0) {
                    wt.setLife(0);
                    t.moveToNeighborTile(tp);
                } else if (lifeOfTile == 1) {
                    wt.setLife(1);
                    t.moveToNeighborTile(tp);
                } else if (lifeOfTile > 1) {
                    t.moveToNeighborTile(tp);
                } else System.out.println("Baj van!");
            }

        } else if (pandatype.equals("J")) {

            System.out.println("Milyen csempén van a Panda? (R-normál tile/W-weak tile)\n");

            while (!tiletype.equals("W") && !tiletype.equals("R")) {
                tiletype = in.nextLine().substring(0, 1).toUpperCase();
            }

            if (tiletype.equals("R")) {
                JumpyPanda jp = new JumpyPanda();
                Tile t1 = new Tile();
                Tile t2 = new Tile();

                t1.addNeighbor(t2);
                t2.addNeighbor(t1);

                t1.take(jp);
                jp.tile = t1;

                t1.moveToNeighborTile(jp);

            } else if (tiletype.equals("W")) {

                System.out.println("Hány élete van a gyenge csempének?\n");
                int lifeOfTile = in.nextInt();

                JumpyPanda jp = new JumpyPanda();
                Tile t = new Tile();
                WeakTile wt = new WeakTile();

                t.addNeighbor(wt);
                wt.addNeighbor(t);
                jp.tile = t;
                t.take(jp);

                if (lifeOfTile == 0) {
                    wt.setLife(0);
                    t.moveToNeighborTile(jp);
                } else if (lifeOfTile == 1) {
                    wt.setLife(1);
                    t.moveToNeighborTile(jp);
                } else if (lifeOfTile > 1) {
                    t.moveToNeighborTile(jp);
                } else System.out.println("Baj van!");
            }
        } else if (pandatype.equals("Z")) {
            System.out.println("Mennyi energiája van a TiredPandának?");
            int energy = in.nextInt();

            if(energy==0) {System.out.println("A Panda kimerült, nem tud lépni!");}

            else if(energy>0){
                System.out.println("Milyen csempén van a Panda? (R-normál tile/W-weak tile)\n");
                Scanner in2 = new Scanner(System.in);
                while (!tiletype.equals("W") && !tiletype.equals("R")) {
                    tiletype = in2.nextLine().substring(0, 1).toUpperCase();
                }

                if (tiletype.equals("R")) {
                    TiredPanda tip = new TiredPanda();
                    Tile t1 = new Tile();
                    Tile t2 = new Tile();

                    t1.addNeighbor(t2);
                    t2.addNeighbor(t1);

                    t1.take(tip);
                    tip.tile = t1;

                    t1.moveToNeighborTile(tip);

                } else if (tiletype.equals("W")) {

                    System.out.println("Hány élete van a gyenge csempének?\n");
                    Scanner number= new Scanner(System.in);
                    int lifeOfTile = number.nextInt();

                    TiredPanda tip = new TiredPanda();
                    Tile t = new Tile();
                    WeakTile wt = new WeakTile();

                    t.addNeighbor(wt);
                    wt.addNeighbor(t);
                    tip.tile = t;
                    t.take(tip);

                    if (lifeOfTile == 0) {
                        wt.setLife(0);
                        t.moveToNeighborTile(tip);
                    } else if (lifeOfTile == 1) {
                        wt.setLife(1);
                        t.moveToNeighborTile(tip);
                    } else if (lifeOfTile > 1) {
                        t.moveToNeighborTile(tip);
                    } else System.out.println("Baj van!");
                }

            } else System.out.println("Baj van!");

        }
    }

    public void szekrenyKifejtiHatasat(){
        System.out.println("D - Szekrény kifejti hatását");
        Scanner sor = new Scanner(System.in);

        Map map = new Map();

        Tile wardrobe1Tile = new Tile();
        Wardrobe wardrobe1 = new Wardrobe();
        wardrobe1.setTile(wardrobe1Tile);
        wardrobe1Tile.addThing(wardrobe1);
        Orangutan orangutan = new Orangutan();
        orangutan.setTile(wardrobe1Tile);
        wardrobe1Tile.setAnimal(orangutan);
        Wardrobe wardrobe2 = new Wardrobe();
        Tile wardrobe2Tile = new Tile();
        wardrobe2.setTile(wardrobe2Tile);
        wardrobe2Tile.addThing(wardrobe2);
        wardrobe1.addOtherWardrobe(wardrobe2);
        wardrobe2.addOtherWardrobe(wardrobe1);

        map.addThing(wardrobe1);
        map.addThing(wardrobe2);
        map.addTile(wardrobe1Tile);
        map.addTile(wardrobe2Tile);
        map.addAnimal(orangutan);

        System.out.println("Az érintett Orangután mögött van Panda?\n");

        System.out.println("Y - Van");
        System.out.println("N - Nincs");

        String answer = "-";

        while(!answer.equals("Y") && !answer.equals("N")){
            answer = sor.nextLine().substring(0, 1).toUpperCase();
        }

        if(answer.equals("Y")) {
            Tile previousTile = wardrobe1Tile;
            Animal previousAnimal = orangutan;

            JumpyPanda tempPanda = new JumpyPanda();
            Tile tempPandaTile = new Tile();
            tempPandaTile.setAnimal(tempPanda);
            tempPanda.setTile(tempPandaTile);
            orangutan.chain(null, tempPanda);
            tempPanda.chain(previousAnimal, null);
            tempPandaTile.addNeighbor(previousTile);
            previousTile.addNeighbor(tempPandaTile);

            map.addTile(tempPandaTile);
            map.addAnimal(tempPanda);

            previousTile = tempPandaTile;
            previousAnimal = tempPanda;

            Boolean morePanda = true;

            while (morePanda) {
                System.out.println("Amögött van Panda?\n");
                answer = "-";

                while (!answer.equals("Y") && !answer.equals("N")) {
                    answer = sor.nextLine().substring(0, 1).toUpperCase();
                }

                if (answer.equals("Y")) {
                    tempPanda = new JumpyPanda();
                    tempPandaTile = new Tile();
                    tempPandaTile.setAnimal(tempPanda);
                    tempPanda.setTile(tempPandaTile);
                    previousAnimal.setAnimalBehind(tempPanda);
                    tempPanda.chain(previousAnimal, null);
                    tempPandaTile.addNeighbor(previousTile);
                    previousTile.addNeighbor(tempPandaTile);

                    map.addTile(tempPandaTile);
                    map.addAnimal(tempPanda);

                    previousTile = tempPandaTile;
                    previousAnimal = tempPanda;

                    morePanda = true;
                } else if (answer.equals("N")) {
                    morePanda = false;
                } else {
                    System.out.println("Baj van");
                }
            }
        } else if(answer.equals("N")){
            // itt semmi nem kell
        } else {
            System.out.println("Baj van");
        }
        wardrobe1.act();
    }

    public void fotelKifejtiHatasat(){
        System.out.println("A - Fotel kifejti hatását");
        Scanner in = new Scanner(System.in);

        System.out.println("Az érintett TiredPandának mennyi energiája van?\n");

        System.out.println("[1-10] - Van még");
        System.out.println("  0    - Nincs");

        int energy = -1;

        while(energy < 0 || energy > 10){
            energy = in.nextInt();
        }

        if(energy > 0){
            TiredPanda tp = new TiredPanda();
            Tile t1 = new Tile();
            Tile t2 = new Tile();
            Armchair a = new Armchair();

            tp.tile = t1;
            t1.take(tp);

            t2.addThing(a);
            a.tile = t2;

            t1.addNeighbor(t2);
            t2.addNeighbor(t1);

            for(int i = 10; i > energy; i--){ //Lower energy
                tp.act();
            }

            a.act();
        } else if (energy == 0){
            System.out.println("A panda sorban van?\n");
            Scanner sor = new Scanner(System.in);

            System.out.println("Y - Igen");
            System.out.println("N - Nem");

            String answer = "-";

            while(!answer.equals("Y") && !answer.equals("N")){
                answer = sor.nextLine().substring(0, 1).toUpperCase();
            }

            if(answer.equals("Y")){
                TiredPanda tp = new TiredPanda();
                JumpyPanda jp = new JumpyPanda();
                Tile t1 = new Tile();
                Tile t2 = new Tile();
                Tile t3 = new Tile();
                Armchair a = new Armchair();

                tp.tile = t1;
                t1.take(tp);

                t2.addThing(a);
                a.tile = t2;

                jp.tile = t3;
                t3.take(jp);

                t1.addNeighbor(t2);
                t2.addNeighbor(t1);
                t1.addNeighbor(t3);
                t3.addNeighbor(t1);

                jp.animalBehind = tp;
                tp.animalAtFront = jp;

                for(int i = 0; i < 10; i++){ //Energy to 0
                    tp.act();
                }

                a.act();
            } else if(answer.equals("N")){
                TiredPanda tp = new TiredPanda();
                Tile t1 = new Tile();
                Tile t2 = new Tile();
                Armchair a = new Armchair();

                tp.tile = t1;
                t1.take(tp);

                t2.addThing(a);
                a.tile = t2;

                t1.addNeighbor(t2);
                t2.addNeighbor(t1);

                for(int i = 0; i < 10; i++){ //Energy to 0
                    tp.act();
                }

                a.act();
            } else {
                System.out.println("Baj van");
            }
        } else {
            System.out.println("Baj van");
        }
    }

    public void jatekgepSipol(){
        System.out.println("S - Játékgép sípol");
        Scanner in = new Scanner(System.in);

        System.out.println("Az érintett TimidPanda sorban van?\n");

        System.out.println("Y - Igen");
        System.out.println("N - Nem");

        String answer = "-";

        while(!answer.equals("Y") && !answer.equals("N")){
            answer = in.nextLine().substring(0, 1).toUpperCase();
        }

        if(answer.equals("Y")){
            Tile t1 = new Tile();
            Tile t2 = new Tile();
            Tile t3 = new Tile();
            SlotMachine sm = new SlotMachine();
            TimidPanda tp = new TimidPanda();
            TiredPanda qp = new TiredPanda();

            t1.addNeighbor(t2);
            t2.addNeighbor(t1);
            t2.addNeighbor(t3);
            t3.addNeighbor(t2);

            t1.addThing(sm);
            sm.tile = t1;

            tp.tile = t2;
            t2.take(tp);

            t3.take(qp);
            qp.tile = t3;

            tp.animalAtFront = qp;
            qp.animalBehind = tp;

            sm.act();
        } else if(answer.equals("N")){
            Tile t1 = new Tile();
            Tile t2 = new Tile();
            SlotMachine sm = new SlotMachine();
            TimidPanda tp = new TimidPanda();

            t1.addNeighbor(t2);
            t2.addNeighbor(t1);

            t1.addThing(sm);
            sm.tile = t1;

            tp.tile = t2;
            t2.take(tp);

            sm.act();
        } else {
            System.out.println("Baj van");
        }
    }

    public void csokiAutomataCsilingel(){
        System.out.println("C - Csokiautomata csilingel");
        Scanner in = new Scanner(System.in);

        System.out.println("Az érintett JumpyPanda alatti csempe milyen?");

        System.out.println("R - Tile");
        System.out.println("W - Weaktile");

        String answer = "-";

        while(!answer.equals("R") && !answer.equals("W")){
            answer = in.nextLine().substring(0, 1).toUpperCase();
        }

        if(answer.equals("R")) {
            Tile t1 = new Tile();
            Tile t2 = new Tile();

            JumpyPanda jp = new JumpyPanda();
            Chocomat cm = new Chocomat();

            t1.addNeighbor(t2);
            t2.addNeighbor(t1);

            jp.tile = t1;
            t1.take(jp);

            t2.addThing(cm);
            cm.tile = t2;

            cm.act();

        } else if(answer.equals("W")){
            System.out.println("Hány élete van a WeakTile-nak?\n");
            Scanner numOfLife = new Scanner(System.in);

            System.out.println("  1    - Egy");
            System.out.println("[2-20] - Több");

            int life= numOfLife.nextInt();

            if(life>=2 && life<=20){
                Tile t = new Tile();
                WeakTile wt = new WeakTile();
                wt.setLife(life);

                JumpyPanda jp = new JumpyPanda();
                Chocomat cm = new Chocomat();

                t.addNeighbor(wt);
                wt.addNeighbor(t);

                jp.tile = wt;
                wt.setPanda(jp);

                t.addThing(cm);
                cm.tile = t;

                cm.act();

            } else if(life == 1){

                System.out.println("Sorban van?");
                Scanner sor = new Scanner(System.in);

                System.out.println("Y - Igen");
                System.out.println("N - Nem");

                answer = "-";

                while(!answer.equals("Y") && !answer.equals("N")){
                    answer = sor.nextLine().substring(0, 1).toUpperCase();
                }

                if(answer.equals("Y")){

                    Tile t1 = new Tile();
                    WeakTile wt = new WeakTile();
                    Tile t2 = new Tile();
                    wt.setLife(1);

                    TimidPanda tp = new TimidPanda();
                    JumpyPanda jp = new JumpyPanda();
                    Chocomat cm = new Chocomat();

                    t1.addNeighbor(wt);
                    wt.addNeighbor(t1);
                    wt.addNeighbor(t2);
                    t2.addNeighbor(wt);

                    tp.tile = t1;
                    t1.take(tp);

                    jp.tile = wt;
                    wt.setPanda(jp);

                    t2.addThing(cm);
                    cm.tile = t2;

                    tp.animalBehind = jp;
                    jp.animalAtFront = tp;

                    cm.act();

                } else if(answer.equals("N")){

                    Tile t = new Tile();
                    WeakTile wt = new WeakTile();
                    wt.setLife(1);

                    JumpyPanda jp = new JumpyPanda();
                    Chocomat cm = new Chocomat();

                    t.addNeighbor(wt);
                    wt.addNeighbor(t);

                    jp.tile = wt;
                    wt.setPanda(jp);

                    t.addThing(cm);
                    cm.tile = t;

                    cm.act();
                } else {
                    System.out.println("Baj van");
                }
            }

        }

    }

    public void tiredPandaMegprobalFelallniAFotelbol(){
        System.out.println("Q - TiredPanda megpróbál felállni a fotelből\n");
        Scanner in = new Scanner(System.in);

        System.out.println("Mennyi energiája van?\n");

        System.out.println("[0-9] - Még nem feltöltődött TiredPanda a fotelben marad és töltődik");
        System.out.println("  10  - Maximális energiájú TiredPanda feláll a fotelből");

        int energy = -1;

        while(energy < 0 || energy > 10){
            energy = in.nextInt();
        }

        if(energy == 10){
            TiredPanda tp = new TiredPanda();
            Tile t1 = new Tile();
            Tile t2 = new Tile();
            Armchair a = new Armchair();

            tp.tile = t1;
            t1.take(tp);

            t2.addThing(a);
            a.tile = t2;

            t1.addNeighbor(t2);
            t2.addNeighbor(t1);

            for(int i = 0; i < 10; i++){ //Energy to 0
                tp.act();
            }

            a.act();

            for(int i = 0; i < 10; i++){ //Energy to 10
                tp.act();
            }

            tp.act();  //Stand
        } else if(energy >= 0 && energy < 10){
            TiredPanda tp = new TiredPanda();
            Tile t1 = new Tile();
            Tile t2 = new Tile();
            Armchair a = new Armchair();

            tp.tile = t1;
            t1.take(tp);

            t2.addThing(a);
            a.tile = t2;

            t1.addNeighbor(t2);
            t2.addNeighbor(t1);

            for(int i = 0; i < 11; i++){ //Energy to 0
                tp.act();
            }

            a.act();

            for(int i = 0; i < 5; i++){
                tp.act();
            }
        } else {
            System.out.println("Baj van");
        }
    }
}
