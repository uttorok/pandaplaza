package com.uttorok.pandaplaza;

/**
 * Interface that is implemented by Animals' and Things' subclasses
 */
public interface IAction {
    void act();
}
