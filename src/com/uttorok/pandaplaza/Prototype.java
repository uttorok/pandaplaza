package com.uttorok.pandaplaza;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

/**
 * UNUSED, DEPRECATED CODE
 */
public class Prototype {

    /**
     * A prototípus futtatására szolgáló fő metódus
     * @throws IOException
     */
    public static void runPrototype() throws IOException {
        Scanner sc = new Scanner(System.in);
        Testcase ts = new Testcase(0); // csak inicializáláshoz

        while(true) {

            try{
                String line = sc.nextLine();

                String[] cmd = line.split(" ");

                if (cmd[0].toUpperCase().equals("Exit".toUpperCase())) {
                    sc.close();
                    System.exit(0);
                } else if (cmd[0].toUpperCase().equals("InputState".toUpperCase())) {
                    inputState(cmd);
                } else if (cmd[0].toUpperCase().equals("LoadState".toUpperCase())) {
                    loadState(cmd);
                } else if (cmd[0].toUpperCase().equals("InputTestcase".toUpperCase())) {
                    inputTestcase(cmd);
                } else if (cmd[0].toUpperCase().equals("LoadTestcase".toUpperCase())) {
                    // all argumentummal mindegyik teszt lefut
                    if(cmd[1].toLowerCase().equals("all")){
                        for(int i = 1; i <= 35; i++){
                            if(i < 10){
                                cmd[1] = "case0" + i;
                            }
                            else{
                                cmd[1] = "case" + i;
                            }
                            System.out.println("\n" + cmd[1] + ".txt");
                            loadTestcase(cmd);
                        }
                    }
                    else{
                        loadTestcase(cmd);
                    }
                } else if (cmd[0].toUpperCase().equals("ExecuteCommand".toUpperCase())) {
                    executeCommand(cmd);
                } else if (cmd[0].toUpperCase().equals("OutputState".toUpperCase())) {
                    outputState();
                } else if (cmd[0].toUpperCase().equals("SaveState".toUpperCase())) {
                    saveState(cmd);
                } else
                    System.out.println("Invalid command");
            } catch (FileNotFoundException e){
                System.out.println(e.toString());
            }
        }
    }

    /**
     * Leírás: A parancsot kiadva, utána begépelve az elvárt pálya állapotát az betöltődik.
     * Opciók: A parancs kiadása után a program inputra vár, melynek formátuma a
     * pálya állapotot leíró formátumnak felel meg. Valid inputot beadva az annak
     * megfelelő állapot betöltődik.
     *
     * @param cmd parameters
     */
    protected static void inputState(String[] cmd) {
    }

    /**
     * Leírás: A parancs egy állapot fájlból történő betöltését végzi el.
     * Opciók: fájlnév: a betölteni kívánt állapotot tartalmazó fájl neve. A fájl formátuma a
     * pálya állapotot leíró formátumnak felel meg. Valid állapotot tartalmazó fájl esetén
     * az annak megfelelő állapot betöltődik.
     *
     * @param cmd parameters
     */
    protected static void loadState(String[] cmd) throws FileNotFoundException{

        // tobb szobol allo file -t is be tudjunk olvasni
        String inputFileName = "";
        for(int i = 1; i < cmd.length; ++i){
            inputFileName += cmd[i];
            if(i != cmd.length - 1) {
                inputFileName += " ";
            }
        }
        String fileName="source/test/cases/" + cmd[1] + ".txt";
        FileReader fr = new FileReader(fileName);
        Scanner sc = new Scanner(fr);

        int idx = 1, size = 0;
        size = sc.nextInt();
        State ts = new Testcase(size); // ez az a Testcase, amivel tovabb tudtok dolgozni
        String tmp = sc.nextLine(); // beolvasasi anomalia miatt

        while (sc.hasNextLine()) {
            if(idx >= 1 && idx <= size) {
                String line = sc.nextLine();
                String[] lineParts = sajatSplit(line);

                int x = idx - 1;
                for(int y = 0; y < size; y++) {

                    if(Integer.valueOf(lineParts[y]) == 0) {
                        ts.setMatrix(x, y, false);
                    } else if (Integer.valueOf(lineParts[y]) == 1) {
                        ts.setMatrix(x, y, true);
                    } else {
                        System.out.println("HIBA - Prototype - loadTestcase fuggveny-ben, a beolvasott szomszedsagi matrix eleme se nem 1, se nem 0");
                    }
                }
            }

            if(idx >= size + 1 && idx <= 2 * size) {
                String line = sc.nextLine();
                String[] lineParts = sajatSplit(line);

                TileInfo ti = new TileInfo(
                        Integer.valueOf(lineParts[0]), // tileNumber
                        lineParts[1],                  // tileType
                        Integer.valueOf(lineParts[2]), // tileParameter
                        lineParts[3],                  // thingOnTile
                        lineParts[4],                  // animalOnTile
                        Integer.valueOf(lineParts[5]), // animalParameter
                        Integer.valueOf(lineParts[6]), // animalFront
                        Integer.valueOf(lineParts[7]), // animalBehind
                        Integer.valueOf(lineParts[8])  // orangutanWaitTime
                );

                ts.addTileInfoToArray(ti);
            }
            if(idx == 2 * size + 1) {
                String line = sc.nextLine();
                String[] lineParts = sajatSplit(line);
            }

            idx++;
        }

        setupCurrentMapTiles(ts);
        setupCurrentMapThings(ts);
        setupCurrentMapAnimals(ts);
        chainAnimals(ts);

        outputState();
        //return ts;
    }

    /**
     * Leírás: A parancsot kiadva, utána begépelve az elvárt tesztesetet az állapot
     * betöltődik és az adott parancs végrehajtódik.
     * Opciók: A parancs kiadása után a program inputra vár, melynek formátuma a
     * tesztesetet leíró formátumnak felel meg. Valid inputot beadva az annak megfelelő
     * állapot betöltődik és az adott parancs végrehajtódik.
     *
     * @param cmd parameters
     */
    protected static void inputTestcase(String[] cmd) {
    }

    /**
     * Leírás: A parancs egy teszteset fájlból történő betöltését végzi el és végrehajtja az
     * abból kiolvasott parancsot.
     * Opciók: fájlnév: a betölteni kívánt tesztesetet tartalmazó fájl neve. A fájl
     * formátuma a tesztesetet leíró formátumnak felel meg. Valid tesztesetet tartalmazó
     * fájl esetén az annak megfelelő állapot betöltődik és a parancs végrehajtódik.
     *
     * @param cmd parameters
     */
    protected static void loadTestcase(String[] cmd) throws FileNotFoundException {

        //String fileName="source/test/cases/" + cmd[1] + ".txt";
        String fileName="input.txt";
        FileReader fr = new FileReader(fileName);
        Scanner sc = new Scanner(fr);

        int idx = 1, size = 0;
        size = sc.nextInt();
        Testcase ts = new Testcase(size); // ez az a Testcase, amivel tovabb tudtok dolgozni
        String tmp = sc.nextLine(); // beolvasasi anomalia miatt

        while (sc.hasNextLine()) {
            if(idx >= 1 && idx <= size) {
                String line = sc.nextLine();
                String[] lineParts = sajatSplit(line);

                int x = idx - 1;
                for(int y = 0; y < size; y++) {

                    if(Integer.valueOf(lineParts[y]) == 0) {
                        ts.setMatrix(x, y, false);
                    } else if (Integer.valueOf(lineParts[y]) == 1) {
                        ts.setMatrix(x, y, true);
                    } else {
                        System.out.println("HIBA - Prototype - loadTestcase fuggveny-ben, a beolvasott szomszedsagi matrix eleme se nem 1, se nem 0");
                    }
                }
            }

            if(idx >= size + 1 && idx <= 2 * size) {
                String line = sc.nextLine();
                String[] lineParts = sajatSplit(line);

                TileInfo ti = new TileInfo(
                        Integer.valueOf(lineParts[0]), // tileNumber
                        lineParts[1],                  // tileType
                        Integer.valueOf(lineParts[2]), // tileParameter
                        lineParts[3],                  // thingOnTile
                        lineParts[4],                  // animalOnTile
                        Integer.valueOf(lineParts[5]), // animalParameter
                        Integer.valueOf(lineParts[6]), // animalFront
                        Integer.valueOf(lineParts[7]), // animalBehind
                        Integer.valueOf(lineParts[8])  // orangutanWaitTime
                );

                ts.addTileInfoToArray(ti);
            }

            if(idx == 2 * size + 1) {
                String line = sc.nextLine();
                String[] lineParts = sajatSplit(line);

                Instruction in = new Instruction(
                        Integer.valueOf(lineParts[0]),
                        lineParts[1],
                        lineParts[2],
                        Integer.valueOf(lineParts[3])
                );

                ts.setInstruction(in);
            }

            idx++;
        }

        setupCurrentMapTiles(ts);
        setupCurrentMapThings(ts);
        setupCurrentMapAnimals(ts);
        chainAnimals(ts);

        ts.getInstruction().execute();

        outputState();
    }

    /**
     * Leírás: A parancs egy függvényhívást végez a betöltve lévő állapoton.
     * Opciók: a parancs opciói a függvényhívás formátumának felelnek meg, tehát:
     * Csempe száma, Milyen típusú objektumon történik a hívás, Függvény neve,
     * Függvény paramétere.
     *
     * @param cmd parameters
     */
    protected static void executeCommand(String[] cmd) {
        try {
            Instruction i = new Instruction(Integer.parseInt(cmd[1]), cmd[2], cmd[3], Integer.parseInt(cmd[4]));
            i.execute();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }

    /**
     * Leírás: A parancs a pálya állapotát leíró formátumnak megfelelő formátumban
     * kiírja a betöltött pálya aktuális állapotát.
     * Opciók: -
     *
     */
    protected static void outputState() {
        State output = new State(Game.getInstance().getCurrentMap().getTiles().size());
        output = createStateWithTiles(output);
        output = createStateWithThings(output);
        output = createStateWithAnimals(output);
        output = createStateWithAnimalsRelations(output);

        int size = output.getTileInfoList().size();

        //Méret
        System.out.println(size);

        //Mátrix
        boolean[][] matrix = output.getMatrix();
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                System.out.print(matrix[i][j]?1:0);
            }
            System.out.println();
        }

        //Csempék
        ArrayList<TileInfo> tileinfoAL = output.getTileInfoList();
        for (int i = 0; i < tileinfoAL.size(); i++){
            System.out.println(tileinfoAL.get(i).getTileNumber() + " " + tileinfoAL.get(i).getTileType() + " "
                    + tileinfoAL.get(i).getTileParameter()       + " " + tileinfoAL.get(i).getThingOnTile() + " "
                    + tileinfoAL.get(i).getAnimalOnTile()        + " " + tileinfoAL.get(i).getAnimalParameter()+ " "
                    + tileinfoAL.get(i).getAnimalFrontTileNum()  + " " + tileinfoAL.get(i).getAnimalBehindTileNum() + " "
                    + tileinfoAL.get(i).getOrangutanWaitTime());
        }
    }

    /**
     * Leírás: A parancs a pálya állapotát leíró formátumnak megfelelő formátumban
     * fájlba írja a betöltött pálya aktuális állapotát.
     * Opciók: fájlnév: a kívánt fájl neve.
     *
     * @param cmd parameters
     */
    protected static void saveState(String[] cmd) throws IOException {

        // tobb szobol allo file -t is be tudjunk olvasni
        String inputFileName = cmd[1];

        FileWriter fw = new FileWriter(inputFileName);

        State output = new State(Game.getInstance().getCurrentMap().getTiles().size());
        output = createStateWithTiles(output);
        output = createStateWithThings(output);
        output = createStateWithAnimals(output);
        output = createStateWithAnimalsRelations(output);

        int size = output.getTileInfoList().size();

        fw.write(size + "\n");

        boolean[][] matrix = output.getMatrix();
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                if(matrix[i][j]) {
                    fw.write("1");
                } else {
                    fw.write("0");
                }
            }
            fw.write("\n");
        }

        ArrayList<TileInfo> tileinfoAL = output.getTileInfoList();
        for (int i = 0; i < tileinfoAL.size(); i++){
            fw.write(tileinfoAL.get(i).getTileNumber() + " " + tileinfoAL.get(i).getTileType() + " " + tileinfoAL.get(i).getTileParameter() + " " + tileinfoAL.get(i).getThingOnTile() + " " + tileinfoAL.get(i).getAnimalOnTile() + " " + tileinfoAL.get(i).getAnimalParameter() + " " + tileinfoAL.get(i).getAnimalFrontTileNum() + " " + tileinfoAL.get(i).getAnimalBehindTileNum() + " " + tileinfoAL.get(i).getOrangutanWaitTime() + "\n");
        }

        fw.close();
    }

    /**
     * Egyedi feldarabolást végez
     * @param line a fájl egy sora
     * @return feldarabolt szöveg
     */
    public static String[] sajatSplit(String line) {
        int reszekSzama = 0;
        for(int i = 0; i < line.length(); i++){
            if(line.charAt(i) != ' ' && i == 0) {
                reszekSzama++;
            }
            if(line.charAt(i) != ' ' && i > 0 && line.charAt(i - 1) == ' ') {
                reszekSzama++;
            }
        }

        String[] tomb = new String[reszekSzama];

        String cica = "";
        int idx = 0;
        for(int i = 0; i < line.length(); i++){
            if(line.charAt(i) != ' ') {
                cica += line.charAt(i);
            }
            if(line.charAt(i) == ' ' && i > 0 && line.charAt(i - 1) != ' ') {
                String macska = cica;
                tomb[idx++] = macska;
                cica = "";
            } else if (i == line.length() - 1 && line.charAt(i) != ' ') {
                String macska = cica;
                tomb[idx++] = macska;
                cica = "";
            }
        }

        return tomb;
    }

    /**
     * A Game Map-jének csempéit létrehozza,
     * beállítja az értékeit és a szomszédait
     *
     * @param s Bemenet állapot
     */
    protected static void setupCurrentMapTiles(State s){
        Map m = new Map();
        ArrayList<TileInfo> tileInfoList = s.getTileInfoList();

        //Tile-ok hozzáadása a Map-hez
        for(int i = 0; i < tileInfoList.size(); i++){
            if(tileInfoList.get(i).getTileType().equals("NormalTile")) {
                m.addTile(new Tile());
            }
            else if(tileInfoList.get(i).getTileType().equals("WeakTile")) {
                WeakTile w = new WeakTile();
                w.setLife(tileInfoList.get(i).getTileParameter());
                m.addTile(w);
            } else if(tileInfoList.get(i).getTileType().equals("Exit")) {
                Exit ex = new Exit();
                m.addTile(ex);
            } else if(tileInfoList.get(i).getTileType().equals("Entrance")) {
                Entrance ent = new Entrance();
                m.addTile(ent);
            } else {
                System.out.println("Baj van! - Prototype / setupCurrentMapTiles - " + tileInfoList.get(i).getTileType().getClass().getSimpleName());
            }
        }

        boolean[][] b = s.getMatrix();

        //Tile-ok szomszédainak beállítása
        for(int i = 0; i < b.length; i++) {
            for(int j = 0; j < b[i].length; j++) {
                if(b[i][j]){
                    m.getTile(i).addNeighbor(m.getTile(j));
                }
            }
        }

        Game.getInstance().setCurrentMap(m);
    }

    /**
     * Beállítja a bemeneti állapot méretét (size), szomszédsági mátrixát (matrix),
     * csempeinformációknál (tileInfoList) pedig a csempeszámot (tileNumber), csempetípust (tileType) és csempeparamétert (tileParameter)
     *
     * @param s Bemenet állapot
     */
    public static State createStateWithTiles(State s) {

        Map m = Game.getInstance().getCurrentMap();
        ArrayList<Tile> tilesOnMap = m.getTiles();
        ArrayList<TileInfo> tileInfoList = new ArrayList<TileInfo>();

        // State méretének beállítása
        State newState = new State(m.getTiles().size());

        // State mátrixának beállítása
        for (int i = 0; i < m.getTiles().size(); i++) {
                for (int j = 0; j < m.getTiles().size(); j++) {
                    if (m.getTile(i).getNeighbors().contains(m.getTile(j))) {
                        newState.setMatrix(i, j, true);
                    } else {
                        newState.setMatrix(i, j, false);
                    }
                }
        }

        for (int i = 0; i < m.getTiles().size(); i++) {
            TileInfo ti = new TileInfo();

            // Csempe szám beállítás
            ti.setTileNumber(i + 1);

            // Csempe típusának beállítása
            if(tilesOnMap.get(i).getClass().getSimpleName().equals("Tile")){
                ti.setTileType("NormalTile");
            }
            else{
                ti.setTileType(tilesOnMap.get(i).getClass().getSimpleName());
            }

            // Csempe élete
            if(ti.getTileType().equals("WeakTile")) {

                ti.setTileParameter(tilesOnMap.get(i).weakLife());
            } else {
                ti.setTileParameter(0);
            }

            tileInfoList.add(ti);
        }

        newState.setTileInfoList(tileInfoList);

        return newState;
    }

    /**
     * Beállítja a Tile-okon lévő tárgyakat: létrehozza őket, majd ráhelyezi a megfelelő csempére.
     *
     * @param s Bemenet állapot
     */
    protected static void setupCurrentMapThings(State s){

        ArrayList<TileInfo> tileInfoList = s.getTileInfoList();
        ArrayList<Wardrobe> wardrobes = new ArrayList<Wardrobe>();

        Map m = Game.getInstance().getCurrentMap();//animalfronttile map.gettile.getanimal

        for(int i = 0; i < tileInfoList.size(); i++){
            if(tileInfoList.get(i).getThingOnTile()!=null){

                if(tileInfoList.get(i).getThingOnTile().equals("Armchair")){
                    Armchair a = new Armchair();
                    a.setTile(m.getTile(i));
                    m.getTile(i).addThing(a);

                } else if(tileInfoList.get(i).getThingOnTile().equals("Chocomat")){
                    Chocomat c = new Chocomat();
                    c.setTile(m.getTile(i));
                    m.getTile(i).addThing(c);

                } else if(tileInfoList.get(i).getThingOnTile().equals("SlotMachine")){
                    SlotMachine sm = new SlotMachine();
                    sm.setTile(m.getTile(i));
                    m.getTile(i).addThing(sm);

                } else if(tileInfoList.get(i).getThingOnTile().equals("Wardrobe")){
                    Wardrobe w = new Wardrobe();
                    w.setTile(m.getTile(i));
                    wardrobes.add(w);
                    m.getTile(i).addThing(w);
                } else if(tileInfoList.get(i).getThingOnTile().equals("None")){
                    // semmi
                } else {
                    System.out.println("Baj van! - Prototype / setupCurrentMapThings - " + tileInfoList.get(i).getThingOnTile().getClass().getSimpleName());
                }
            }
        }

        for(int i = 0; i < tileInfoList.size(); i++){
            if(tileInfoList.get(i).getThingOnTile().equals("Wardrobe")) {
                m.getTile(i).getThing().setWardrobes(wardrobes);
            }
        }

        Game.getInstance().setCurrentMap(m);
    }

    /**
     * Beállítja az állapotban a tárgyakat kimentés előtt, feltételezi, hogy a Tile-okat már tartalmazza
     *
     * @param s Kimenteni készült állapot
     */
    public static State createStateWithThings(State s){

        Map m = Game.getInstance().getCurrentMap();
        ArrayList<Tile> tilesOnMap = m.getTiles();
        ArrayList<TileInfo> tileInfoList = s.getTileInfoList();

        for(int i = 0; i < tilesOnMap.size(); i++) {

            if(tilesOnMap.get(i).getThing() != null){
                if(tilesOnMap.get(i).getThing().getClass().getSimpleName().equals("Armchair"))
                    tileInfoList.get(i).setThingOnTile("Armchair");

                else if(tilesOnMap.get(i).getThing().getClass().getSimpleName().equals("Chocomat"))
                    tileInfoList.get(i).setThingOnTile("Chocomat");

                else if(tilesOnMap.get(i).getThing().getClass().getSimpleName().equals("SlotMachine"))
                    tileInfoList.get(i).setThingOnTile("SlotMachine");

                else if(tilesOnMap.get(i).getThing().getClass().getSimpleName().equals("Wardrobe"))
                    tileInfoList.get(i).setThingOnTile("Wardrobe");

            } else {
                tileInfoList.get(i).setThingOnTile("None");
                }
        }


        s.setTileInfoList(tileInfoList);
        return s;
    }

    /**
     * Beállítja a Tile-okon lévő állatokat: létrehozza őket, majd ráhelyezi a megfelelő csempére.
     *
     * @param s Bemenet állapot
     */
    protected static void setupCurrentMapAnimals(State s){

        ArrayList<TileInfo> tileInfoList = s.getTileInfoList();

        //Átvesszük a game-ből az aktuális pályát
        Map m = Game.getInstance().getCurrentMap();

        //Az átvett pályát frissítjük, az állatokkal
        for(int i = 0; i < tileInfoList.size(); i++){
            if(tileInfoList.get(i).getAnimalOnTile()!=null){

                if(tileInfoList.get(i).getAnimalOnTile().equals("None")){
                    // Orangutan o = new Orangutan();
                    // m.getTile(i).setAnimal(o);
                } else if(tileInfoList.get(i).getAnimalOnTile().equals("JumpyPanda")){
                    JumpyPanda jp = new JumpyPanda();
                    jp.setTile(m.getTile(i));
                    m.getTile(i).setAnimal(jp);

                } else if(tileInfoList.get(i).getAnimalOnTile().equals("TimidPanda")){
                    TimidPanda tp = new TimidPanda();
                    tp.setTile(m.getTile(i));
                    m.getTile(i).setAnimal(tp);

                } else if(tileInfoList.get(i).getAnimalOnTile().equals("TiredPanda")){
                    TiredPanda tp = new TiredPanda();
                    tp.setEnergy(tileInfoList.get(i).getAnimalParameter());
                    if(m.getTile(i).getThing() != null) {
                        if(m.getTile(i).getThing().getClass().getSimpleName().equals("Armchair")) {
                            tp.setArmchair(((Armchair)m.getTile(i).getThing()));
                            m.getTile(i).getThing().setTiredPanda(tp);
                        }
                    }
                    tp.setTile(m.getTile(i));
                    m.getTile(i).setAnimal(tp);
                } else if(tileInfoList.get(i).getAnimalOnTile().equals("Orangutan1")){
                    Orangutan o = new Orangutan();
                    o.setTile(m.getTile(i));
                    o.setRemainingSteps(tileInfoList.get(i).getAnimalParameter());
                    o.setWaitTime(tileInfoList.get(i).getOrangutanWaitTime());
                    m.getTile(i).setAnimal(o);
                    Game.getInstance().setOrangutan(1, o);
                } else if(tileInfoList.get(i).getAnimalOnTile().equals("Orangutan2")){
                    Orangutan o = new Orangutan();
                    o.setTile(m.getTile(i));
                    o.setRemainingSteps(tileInfoList.get(i).getAnimalParameter());
                    o.setWaitTime(tileInfoList.get(i).getOrangutanWaitTime());
                    m.getTile(i).setAnimal(o);
                    Game.getInstance().setOrangutan(2, o);
                } else {
                    System.out.println("Baj van! - Prototype / setupCurrentMapAnimals - " + tileInfoList.get(i).getAnimalOnTile().getClass().getSimpleName());
                }

            }
        }

        //A frissített pályát visszatöltjük a game-be
        Game.getInstance().setCurrentMap(m);
    }

    /**
     * A pályán lévő állatokat felfűzi láncba, a bemeneten megadott módon
     * @param s Bemenet állapot
     */
    protected static void chainAnimals(State s){
        ArrayList<TileInfo> tileInfoList = s.getTileInfoList();

        //Átvesszük a game-ből az aktuális pályát
        Map m = Game.getInstance().getCurrentMap();

        //Az átvett pályát frissítjük, az állatokkal
        for(int i = 0; i < tileInfoList.size(); i++) {
                if(tileInfoList.get(i).getAnimalFrontTileNum() > 0){

                    m.getTile(i).getAnimal().animalAtFront = m.getTile(tileInfoList.get(i).getAnimalFrontTileNum()-1).getAnimal();
                }
                if(tileInfoList.get(i).getAnimalBehindTileNum() > 0) {

                    m.getTile(i).getAnimal().animalBehind = m.getTile(tileInfoList.get(i).getAnimalBehindTileNum()-1).getAnimal();
                }
        }

        //A frissített pályát visszatöltjük a game-be
        Game.getInstance().setCurrentMap(m);
    }

    /**
     * Állatok kapcsolatainak state-be mentése
     * @param s Bemeneti állapot
     * @return Visszatérési állapot
     */
    public static State createStateWithAnimalsRelations(State s){
        Map m = Game.getInstance().getCurrentMap();
        ArrayList<Tile> tilesOnMap = m.getTiles();
        ArrayList<TileInfo> tileInfoList = s.getTileInfoList();

        for(int i=0; i<tilesOnMap.size(); i++) {
            if(tilesOnMap.get(i).getAnimal() != null) {
                if(tilesOnMap.get(i).getAnimal().animalAtFront != null){
                    int frontAnimalTileNum = 0;
                    for(int j=0;j<tilesOnMap.size();j++){
                        if(tilesOnMap.get(j)==tilesOnMap.get(i).getAnimal().animalAtFront.getTile()) {
                            frontAnimalTileNum = j + 1;
                            break;
                        }
                    }

                    TileInfo ti = tileInfoList.get(i);
                    ti.setAnimalFrontTileNum(frontAnimalTileNum);
                    tileInfoList.set(i,ti);
                }
                if(tilesOnMap.get(i).getAnimal().animalBehind != null){
                    TileInfo ti=tileInfoList.get(i);
                    int behindAnimalTileNum = 0;
                    for(int j=0;j<tilesOnMap.size();j++){
                        if(tilesOnMap.get(j)==tilesOnMap.get(i).getAnimal().animalBehind.getTile()) {
                            behindAnimalTileNum = j + 1;
                            break;
                        }

                    }
                    ti.setAnimalBehindTileNum(behindAnimalTileNum);
                    tileInfoList.set(i,ti);
                }
            }
        }
        s.setTileInfoList(tileInfoList);
        return s;
    }

    /**
     * Beállítja az állapotban az állatokat kimentés előtt, feltételezi, hogy a Tile-okat már tartalmazza
     *
     * @param s Kimenteni készült állapot
     */
    public static State createStateWithAnimals(State s){

        Map m = Game.getInstance().getCurrentMap();
        ArrayList<Tile> tilesOnMap = m.getTiles();
        ArrayList<TileInfo> tileInfoList = s.getTileInfoList();

        int orangutan1TileNum = 0;

        for(int i = 0; i < tilesOnMap.size(); i++) {
            if(Game.getInstance().getOrangutans().get(0) != null) {
                if (tilesOnMap.get(i).equals(Game.getInstance().getOrangutans().get(0).getTile())) {
                    orangutan1TileNum = i;
                }
            }
        }

        for(int i=0; i < tilesOnMap.size(); i++) {

            if(tilesOnMap.get(i).getAnimal() == null) {
                    tileInfoList.get(i).setAnimalOnTile("None");
            }
            else if (tilesOnMap.get(i).getAnimal().getClass().getSimpleName().equals("JumpyPanda"))
                tileInfoList.get(i).setAnimalOnTile("JumpyPanda");

            else if (tilesOnMap.get(i).getAnimal().getClass().getSimpleName().equals("TimidPanda"))
                tileInfoList.get(i).setAnimalOnTile("TimidPanda");

            else if (tilesOnMap.get(i).getAnimal().getClass().getSimpleName().equals("TiredPanda")) {
                tileInfoList.get(i).setAnimalOnTile("TiredPanda");
                tileInfoList.get(i).setAnimalParameter(tilesOnMap.get(i).getAnimal().getEnergy());
            }

            else if (tilesOnMap.get(i).getAnimal().getClass().getSimpleName().equals("Orangutan")){
                if(orangutan1TileNum == i) {
                    tileInfoList.get(i).setAnimalOnTile("Orangutan1");
                }
                else {
                    tileInfoList.get(i).setAnimalOnTile("Orangutan2");
                }
                tileInfoList.get(i).setAnimalParameter(tilesOnMap.get(i).getAnimal().getLife());
                tileInfoList.get(i).setOrangutanWaitTime(tilesOnMap.get(i).getAnimal().getWaitTime());
            }

            else {
                System.out.println("Baj van! - Prototype / createStateWithAnimals - " + tilesOnMap.get(i).getAnimal().getClass().getSimpleName());
            }
        }

        s.setTileInfoList(tileInfoList);

        return s;
    }

}
