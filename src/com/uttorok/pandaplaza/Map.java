package com.uttorok.pandaplaza;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Class that holds game objects (tiles, animals, things)
 */
public class Map extends JFrame {
    private JMenuBar mb = new JMenuBar();

    private JMenu game = new JMenu("Game");
    private JMenuItem exit = new JMenuItem("Exit");

    private JMenu info = new JMenu("Info");
    private JMenuItem about = new JMenuItem("About");

    private ArrayList<Tile> tiles = new ArrayList<>();
    private ArrayList<Animal> animals = new ArrayList<>();
    private ArrayList<Thing> things = new ArrayList<>();

    private final double screenScale = 1;

    private JLabel lOrangutan1 = new JLabel("Harambe:");
    private JLabel lOrangutan2 = new JLabel("Joseph:");

    private JLabel lStep1 = new JLabel("Steps:");
    private JLabel lStep2 = new JLabel("Steps:");

    private JLabel lPOrangutan1 = new JLabel();
    private JLabel lPOrangutan2 = new JLabel();

    private JLabel lStepOrangutan1 = new JLabel();
    private JLabel lStepOrangutan2 = new JLabel();

    private JButton orangutanElengedes = new JButton("Release");

    /**
     * Default constructor
     */
    public Map() {

        try {
            BufferedImage bf = ImageIO.read(new File("source\\images\\background\\background.jpg"));
            Image i = bf.getScaledInstance((int) (1500 * screenScale), (int) (885 * screenScale), Image.SCALE_SMOOTH);
            setContentPane(new backImage(i));
        } catch (IOException e) {
            e.printStackTrace();
        }

        setupMap();

        ActionListener al = new MenuItemActionListener(this);
        exit.setActionCommand("exit");
        exit.addActionListener(al);
        about.setActionCommand("about");
        about.addActionListener(al);

        setJMenuBar(mb);
        mb.add(game);
        game.add(exit);

        mb.add(info);
        info.add(about);

        setLayout(null);
        setTitle("Pandaplaza");
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(new Dimension((int) (1500 * screenScale), (int) (885 * screenScale) + 65));
        setVisible(true);
        setResizable(false);
        setIconImage(new ImageIcon("source\\images\\icon\\panda_icon.png").getImage());
    }

    Color brown = new Color(245, 222, 179),
            green = new Color(130, 255, 130),
            red = new Color(255, 130, 130),
            blue = new Color(130, 130, 255);

    /**
     * @param buttonX coordinate
     * @param buttonY coordinate
     * @param type type of tile
     * @return the correct tile type
     */
    private Tile tileWithButton(int buttonX, int buttonY, String type) {
        //Tile t;
        switch (type) {
            case "Normal":
                Tile t = new Tile();
                t.getButton().setBackground(brown);
                t.getButton().setFont(new Font("Segoe UI", Font.PLAIN, (int) (16 * (screenScale * 0.95))));
                t.getButton().setFocusPainted(false);
                t.setButtonParameters(buttonX, buttonY, screenScale);
                t.getButton().setOpaque(true);
                t.getButton().setBorderPainted(false);
                add(t.getButton());
                return t;
            case "Weak":
                WeakTile w = new WeakTile();
                w.getButton().setBackground(blue);
                w.getButton().setFont(new Font("Segoe UI", Font.PLAIN, (int) (16 * (screenScale * 0.95))));
                w.getButton().setFocusPainted(false);
                w.setButtonParameters(buttonX, buttonY, screenScale);
                w.getButton().setOpaque(true);
                w.getButton().setBorderPainted(false);
                add(w.getButton());
                return w;
            case "Entrance":
                Entrance e = new Entrance();
                e.getButton().setBackground(green);
                e.getButton().setFont(new Font("Segoe UI", Font.PLAIN, (int) (16 * (screenScale * 0.95))));
                e.getButton().setFocusPainted(false);
                e.setButtonParameters(buttonX, buttonY, screenScale);
                e.getButton().setOpaque(true);
                e.getButton().setBorderPainted(false);
                add(e.getButton());
                return e;
            default: //Exit
                Exit x = new Exit();
                x.getButton().setBackground(red);
                x.getButton().setFont(new Font("Segoe UI", Font.PLAIN, (int) (16 * (screenScale * 0.95))));
                x.getButton().setFocusPainted(false);
                x.setButtonParameters(buttonX, buttonY, screenScale);
                x.getButton().setOpaque(true);
                x.getButton().setBorderPainted(false);
                add(x.getButton());
                return x;
        }

    }

    /**
     * Create labels with default values
     */
    private void createLabels() {
        lOrangutan1.setBounds((int) (1250 * screenScale), (int) (750 * screenScale), 70, 20);
        lOrangutan2.setBounds((int) (1250 * screenScale), (int) (800 * screenScale), 70, 20);

        lPOrangutan1.setBounds((int) (1350 * screenScale), (int) (750 * screenScale), 20, 20);
        lPOrangutan2.setBounds((int) (1350 * screenScale), (int) (800 * screenScale), 20, 20);

        lStep1.setBounds((int) (1375 * screenScale), (int) (750 * screenScale), 60, 20);
        lStep2.setBounds((int) (1375 * screenScale), (int) (800 * screenScale), 60, 20);

        lStepOrangutan1.setBounds((int) (1440 * screenScale), (int) (750 * screenScale), 20, 20);
        lStepOrangutan2.setBounds((int) (1440 * screenScale), (int) (800 * screenScale), 20, 20);

        lPOrangutan1.setText("0");
        lPOrangutan2.setText("0");

        lStepOrangutan1.setText("20");
        lStepOrangutan2.setText("20");

        add(lOrangutan1);
        add(lOrangutan2);
        add(lPOrangutan1);
        add(lPOrangutan2);
        add(lStep1);
        add(lStep2);
        add(lStepOrangutan1);
        add(lStepOrangutan2);
    }

    /**
     * Release button for Orangutans to release Pandas
     */
    private void createOrangutanRelease() {
        orangutanElengedes.setFont(new Font("Segoe UI", Font.PLAIN, (int) (16 * (screenScale * 0.95))));
        orangutanElengedes.setBounds((int) (1350 * screenScale), (int) (700 * screenScale), (int) (110 * screenScale), 25);
        orangutanElengedes.setOpaque(true);

        ActionListener ebal = new ReleaseButtonActionListener();
        orangutanElengedes.addActionListener(ebal);

        add(orangutanElengedes);
    }

    /**
     * Makes all things that can, act
     */
    public void makeAct() {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": makeAct()");

        for (Animal a : animals) {
            a.act();
        }

        for (Thing t : things) {
            t.act();
        }

        reDraw();

        Indentation.decreaseDepth();
    }

    public void addTile(Tile t) {
        tiles.add(t);
    }

    public void addAnimal(Animal a) {
        animals.add(a);
    }

    public void addThing(Thing t) {
        things.add(t);
    }

    public Tile getTile(int i) {
        return tiles.get(i);
    }

    public ArrayList<Tile> getTiles() {
        return tiles;
    }

    public JFrame getFrame() {
        return this;
    }

    /**
     * This method builds up the whole game in the beginning
     */
    public void setupMap() {
        createLabels();
        createOrangutanRelease();

        String normal = "Normal", weak = "Weak", entrance = "Entrance", exit = "Exit";

        // create tiles

        Tile t1 = tileWithButton((int) (0 * screenScale), (int) (50 * screenScale), normal);
        Entrance t2 = (Entrance)tileWithButton((int) (50 * screenScale), (int) (225 * screenScale), entrance);
        Tile t3 = tileWithButton((int) (120 * screenScale), (int) (70 * screenScale), normal);
        Tile t4 = tileWithButton((int) (100 * screenScale), (int) (370 * screenScale), normal);
        Tile t5 = tileWithButton((int) (205 * screenScale), (int) (360 * screenScale), normal);
        Tile t6 = tileWithButton((int) (225 * screenScale), (int) (255 * screenScale), normal);
        Tile t7 = tileWithButton((int) (225 * screenScale), (int) (135 * screenScale), normal);
        WeakTile t8 = (WeakTile)tileWithButton((int) (270 * screenScale), (int) (460 * screenScale), weak);
        Tile t9 = tileWithButton((int) (370 * screenScale), (int) (360 * screenScale), normal);
        Tile t10 = tileWithButton((int) (385 * screenScale), (int) (195 * screenScale), normal);
        Tile t11 = tileWithButton((int) (480 * screenScale), (int) (40 * screenScale), normal);
        Tile t12 = tileWithButton((int) (330 * screenScale), (int) (30 * screenScale), normal);
        Tile t13 = tileWithButton((int) (600 * screenScale), (int) (80 * screenScale), normal);
        Tile t14 = tileWithButton((int) (730 * screenScale), (int) (35 * screenScale), normal);
        Tile t15 = tileWithButton((int) (695 * screenScale), (int) (195 * screenScale), normal);
        Tile t16 = tileWithButton((int) (510 * screenScale), (int) (400 * screenScale), normal);
        Tile t17 = tileWithButton((int) (650 * screenScale), (int) (400 * screenScale), normal);
        Tile t18 = tileWithButton((int) (795 * screenScale), (int) (440 * screenScale), normal);
        Tile t19 = tileWithButton((int) (885 * screenScale), (int) (60 * screenScale), normal);
        Tile t20 = tileWithButton((int) (955 * screenScale), (int) (155 * screenScale), normal);
        Tile t21 = tileWithButton((int) (955 * screenScale), (int) (255 * screenScale), normal);
        Tile t22 = tileWithButton((int) (965 * screenScale), (int) (410 * screenScale), normal);
        Tile t23 = tileWithButton((int) (1085 * screenScale), (int) (350 * screenScale), normal);
        Exit t24 = (Exit)tileWithButton((int) (1110 * screenScale), (int) (230 * screenScale), exit);
        Tile t25 = tileWithButton((int) (1080 * screenScale), (int) (100 * screenScale), normal);
        Tile t26 = tileWithButton((int) (1010 * screenScale), (int) (0 * screenScale), normal);
        Tile t27 = tileWithButton((int) (1240 * screenScale), (int) (0 * screenScale), normal);
        Tile t28 = tileWithButton((int) (1280 * screenScale), (int) (110 * screenScale), normal);
        Tile t29 = tileWithButton((int) (1350 * screenScale), (int) (220 * screenScale), normal);
        Tile t30 = tileWithButton((int) (1280 * screenScale), (int) (360 * screenScale), normal);
        WeakTile t31 = (WeakTile)tileWithButton((int) (1040 * screenScale), (int) (525 * screenScale), weak);
        Tile t32 = tileWithButton((int) (1220 * screenScale), (int) (615 * screenScale), normal);
        Tile t33 = tileWithButton((int) (1110 * screenScale), (int) (730 * screenScale), normal);
        Tile t34 = tileWithButton((int) (930 * screenScale), (int) (720 * screenScale), normal);
        Tile t35 = tileWithButton((int) (690 * screenScale), (int) (715 * screenScale), normal);
        Tile t36 = tileWithButton((int) (780 * screenScale), (int) (600 * screenScale), normal);
        Tile t37 = tileWithButton((int) (560 * screenScale), (int) (635 * screenScale), normal);
        Tile t38 = tileWithButton((int) (400 * screenScale), (int) (655 * screenScale), normal);
        Tile t39 = tileWithButton((int) (400 * screenScale), (int) (520 * screenScale), normal);
        Tile t40 = tileWithButton((int) (255 * screenScale), (int) (645 * screenScale), normal);
        Tile t41 = tileWithButton((int) (120 * screenScale), (int) (500 * screenScale), normal);
        Tile t42 = tileWithButton((int) (0 * screenScale), (int) (370 * screenScale), normal);


        // create animals

        Orangutan a1 = new Orangutan();
        Orangutan a2 = new Orangutan();
        Panda a3 = new TimidPanda();
        Panda a4 = new JumpyPanda();
        Panda a5 = new TiredPanda();
        Panda a6 = new TimidPanda();
        Panda a7 = new JumpyPanda();
        Panda a8 = new TiredPanda();


        // create things

        Thing thing1 = new SlotMachine();
        Wardrobe thing2 = new Wardrobe();
        Thing thing3 = new Chocomat();
        Wardrobe thing4 = new Wardrobe();
        Thing thing5 = new Armchair();

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        // Set tile neighbors, things and animals

        t1.addNeighbor(t2);
        t1.addNeighbor(t3);

        t2.addNeighbor(t1);
        t2.addNeighbor(t3);
        t2.addNeighbor(t7);
        t2.addNeighbor(t6);
        t2.addNeighbor(t5);
        t2.addNeighbor(t4);
        t2.addNeighbor(t42);

        t3.addNeighbor(t1);
        t3.addNeighbor(t2);
        t3.addNeighbor(t7);
        t3.animal = a1;
        a1.tile = t3;


        t4.addNeighbor(t2);
        t4.addNeighbor(t5);
        t4.addNeighbor(t41);
        t4.addNeighbor(t42);

        t5.addNeighbor(t2);
        t5.addNeighbor(t4);
        t5.addNeighbor(t6);
        t5.addNeighbor(t8);

        t6.addNeighbor(t2);
        t6.addNeighbor(t7);
        t6.addNeighbor(t10);
        t6.addNeighbor(t9);
        t6.addNeighbor(t5);

        t7.addNeighbor(t2);
        t7.addNeighbor(t3);
        t7.addNeighbor(t12);
        t7.addNeighbor(t10);
        t7.addNeighbor(t6);

        t8.addNeighbor(t5);
        t8.addNeighbor(t9);
        t8.addNeighbor(t39);
        t8.addNeighbor(t40);
        t8.addNeighbor(t41);

        t9.addNeighbor(t6);
        t9.addNeighbor(t10);
        t9.addNeighbor(t16);
        t9.addNeighbor(t8);

        t10.addNeighbor(t6);
        t10.addNeighbor(t7);
        t10.addNeighbor(t12);
        t10.addNeighbor(t11);
        t10.addNeighbor(t13);
        t10.addNeighbor(t15);
        t10.addNeighbor(t17);
        t10.addNeighbor(t16);
        t10.addNeighbor(t9);

        t11.addNeighbor(t10);
        t11.addNeighbor(t12);
        t11.addNeighbor(t13);

        t12.addNeighbor(t7);
        t12.addNeighbor(t10);
        t12.addNeighbor(t11);
        t12.animal = a3;
        a3.tile = t12;


        t13.addNeighbor(t10);
        t13.addNeighbor(t11);
        t13.addNeighbor(t14);
        t13.addNeighbor(t15);
        t13.animal = a4;
        a4.tile = t13;


        t14.addNeighbor(t13);
        t14.addNeighbor(t15);
        t14.addNeighbor(t19);

        t15.addNeighbor(t10);
        t15.addNeighbor(t13);
        t15.addNeighbor(t14);
        t15.addNeighbor(t19);
        t15.addNeighbor(t20);
        t15.addNeighbor(t21);
        t15.addNeighbor(t22);
        t15.addNeighbor(t18);
        t15.addNeighbor(t17);
        t15.addThing(thing1);
        thing1.tile = t15;

        t16.addNeighbor(t9);
        t16.addNeighbor(t10);
        t16.addNeighbor(t17);
        t16.addNeighbor(t37);
        t16.addNeighbor(t39);
        t16.animal = a5;
        a5.tile = t16;


        t17.addNeighbor(t10);
        t17.addNeighbor(t15);
        t17.addNeighbor(t18);
        t17.addNeighbor(t37);
        t17.addNeighbor(t16);

        t18.addNeighbor(t15);
        t18.addNeighbor(t22);
        t18.addNeighbor(t36);
        t18.addNeighbor(t37);
        t18.addNeighbor(t17);

        t19.addNeighbor(t14);
        t19.addNeighbor(t15);
        t19.addNeighbor(t20);
        t19.addNeighbor(t26);

        t20.addNeighbor(t15);
        t20.addNeighbor(t19);
        t20.addNeighbor(t25);
        t20.addNeighbor(t24);
        t20.addNeighbor(t21);

        t21.addNeighbor(t15);
        t21.addNeighbor(t20);
        t21.addNeighbor(t24);
        t21.addNeighbor(t23);
        t21.addNeighbor(t22);

        t22.addNeighbor(t15);
        t22.addNeighbor(t21);
        t22.addNeighbor(t31);
        t22.addNeighbor(t18);

        t23.addNeighbor(t21);
        t23.addNeighbor(t24);
        t23.addNeighbor(t30);
        t23.addNeighbor(t31);

        t24.addNeighbor(t20);
        t24.addNeighbor(t25);
        t24.addNeighbor(t28);
        t24.addNeighbor(t29);
        t24.addNeighbor(t30);
        t24.addNeighbor(t23);
        t24.addNeighbor(t21);
        t24.addNeighbor(t2);

        t25.addNeighbor(t20);
        t25.addNeighbor(t26);
        t25.addNeighbor(t28);
        t25.addNeighbor(t24);
        t25.animal = a7;
        a7.tile = t25;


        t26.addNeighbor(t19);
        t26.addNeighbor(t25);
        t26.addNeighbor(t27);

        t27.addNeighbor(t26);
        t27.addNeighbor(t28);

        t28.addNeighbor(t24);
        t28.addNeighbor(t25);
        t28.addNeighbor(t27);
        t28.animal = a8;
        a8.tile = t28;


        t29.addNeighbor(t24);
        t29.addNeighbor(t30);
        t29.addThing(thing4);
        thing4.tile = t29;

        t30.addNeighbor(t23);
        t30.addNeighbor(t24);
        t30.addNeighbor(t29);
        t30.addNeighbor(t32);

        t31.addNeighbor(t22);
        t31.addNeighbor(t23);
        t31.addNeighbor(t32);
        t31.addNeighbor(t34);
        t31.addNeighbor(t36);

        t32.addNeighbor(t30);
        t32.addNeighbor(t31);
        t32.addNeighbor(t33);

        t33.addNeighbor(t32);
        t33.addNeighbor(t34);
        t33.addThing(thing5);
        thing5.tile = t33;

        t34.addNeighbor(t31);
        t34.addNeighbor(t33);
        t34.addNeighbor(t35);
        t34.animal = a6;
        a6.tile = t34;


        t35.addNeighbor(t34);
        t35.addNeighbor(t36);
        t35.addNeighbor(t37);

        t36.addNeighbor(t18);
        t36.addNeighbor(t31);
        t36.addNeighbor(t35);
        t36.addNeighbor(t37);

        t37.addNeighbor(t16);
        t37.addNeighbor(t17);
        t37.addNeighbor(t18);
        t37.addNeighbor(t36);
        t37.addNeighbor(t35);
        t37.addNeighbor(t38);
        t37.addNeighbor(t39);
        t37.addThing(thing3);
        thing3.tile = t37;

        t38.addNeighbor(t37);
        t38.addNeighbor(t39);
        t38.addNeighbor(t40);

        t39.addNeighbor(t8);
        t39.addNeighbor(t16);
        t39.addNeighbor(t37);
        t39.addNeighbor(t38);

        t40.addNeighbor(t8);
        t40.addNeighbor(t38);
        t40.addThing(thing2);
        thing2.tile = t40;

        t41.addNeighbor(t4);
        t41.addNeighbor(t8);
        t41.animal = a2;
        a2.tile = t41;


        t42.addNeighbor(t2);
        t42.addNeighbor(t4);

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        // Add tiles to list

        tiles.add(t1);
        tiles.add(t2);
        tiles.add(t3);
        tiles.add(t4);
        tiles.add(t5);
        tiles.add(t6);
        tiles.add(t7);
        tiles.add(t8);
        tiles.add(t9);
        tiles.add(t10);
        tiles.add(t11);
        tiles.add(t12);
        tiles.add(t13);
        tiles.add(t14);
        tiles.add(t15);
        tiles.add(t16);
        tiles.add(t17);
        tiles.add(t18);
        tiles.add(t19);
        tiles.add(t20);
        tiles.add(t21);
        tiles.add(t22);
        tiles.add(t23);
        tiles.add(t24);
        tiles.add(t25);
        tiles.add(t26);
        tiles.add(t27);
        tiles.add(t28);
        tiles.add(t29);
        tiles.add(t30);
        tiles.add(t31);
        tiles.add(t32);
        tiles.add(t33);
        tiles.add(t34);
        tiles.add(t35);
        tiles.add(t36);
        tiles.add(t37);
        tiles.add(t38);
        tiles.add(t39);
        tiles.add(t40);
        tiles.add(t41);
        tiles.add(t42);


        // add animals to list

        animals.add(a1);
        animals.add(a2);
        animals.add(a3);
        animals.add(a4);
        animals.add(a5);
        animals.add(a6);
        animals.add(a7);
        animals.add(a8);


        // add things to list

        things.add(thing1);
        things.add(thing2);
        things.add(thing3);
        things.add(thing4);
        things.add(thing5);

        // set oragutan identifiers
        a1.setName("Harambe");
        a2.setName("Joseph");

        // set orangutans in game
        Game.getInstance().addOrangutan(a1);
        Game.getInstance().addOrangutan(a2);

        // set wardrobe neighbors
        thing2.addOtherWardrobe(thing4);
        thing4.addOtherWardrobe(thing2);

        reDraw();

    }

    /**
     * Redraw the GUI items
     */
    public void reDraw(){
        for(Tile t :tiles) t.setButtonIMG(null);
        for(Animal a: animals) a.draw(screenScale);

        refreshPoints();
        refreshSteps();
    }

    /**
     * Disable all clickable buttons (tiles) on the map
     */
    public void disableButtons() {
        for (Tile t: tiles) {
            t.disableButton();
        }
        reDraw();
    }

    /**
     * Refresh label texts for points
     */
    public void refreshPoints() {
        int jelenlegiPontszamOrangutan1 = Game.getInstance().getPointsOfPlayer1();
        int jelenlegiPontszamOrangutan2 = Game.getInstance().getPointsOfPlayer2();

        lPOrangutan1.setText(Integer.toString(jelenlegiPontszamOrangutan1));
        lPOrangutan2.setText(Integer.toString(jelenlegiPontszamOrangutan2));
    }

    /**
     * Refresh label texts for Orangutan remaining steps
     */
    public void refreshSteps() {

        switch(Game.getInstance().getOrangutans().size()) {
            case 0:
                lStepOrangutan1.setText("0");
                lStepOrangutan2.setText("0");
                break;
            case 1:
                if(Game.getInstance().getOrangutans().get(0).getName().equals("Harambe")) {
                    lStepOrangutan1.setText(Integer.toString(Game.getInstance().getOrangutans().get(0).getRemainingSteps()));
                }
                else {
                    lStepOrangutan2.setText(Integer.toString(Game.getInstance().getOrangutans().get(0).getRemainingSteps()));
                }
                break;
            case 2:
                lStepOrangutan1.setText(Integer.toString(Game.getInstance().getOrangutans().get(0).getRemainingSteps()));
                lStepOrangutan2.setText(Integer.toString(Game.getInstance().getOrangutans().get(1).getRemainingSteps()));
                break;
            default:
                lStepOrangutan1.setText("0");
                lStepOrangutan2.setText("0");
                break;
        }
    }

    /**
     * @param a the Animal to get removed from the list
     */
    public void removeAnimal(Animal a) {
        animals.remove(a);
    }
}

/**
 * Background image
 */
class backImage extends JComponent {
    Image i;

    //Creating Constructer
    public backImage(Image i) {
        this.i = i;
    }

    //Overriding the paintComponent method
    @Override
    public void paintComponent(Graphics g) {

        g.drawImage(i, 0, 0, null);  // Drawing image using drawImage method
    }
}
