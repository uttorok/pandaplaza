package com.uttorok.pandaplaza;

/**
 * Osztály a tesztesetek végén illetve a parancssorból kiadott metódushívások kezelésére
 */
public class Instruction {

    /**
     * Az érintett csempe száma
     */
    private int tileNum;

    /**
     * Az érintett objektum típusa (Thing, Animal, Tile)
     */
    private String typeOfObject;

    /**
     * A hívott metódus neve
     */
    private String method;

    /**
     * A hívott metódus paramétere
     */
    private int parameter;

    public int getTileNum() {
        return tileNum;
    }

    public void setTileNum(int tileNum) {
        this.tileNum = tileNum;
    }

    public String getTypeOfObject() {
        return typeOfObject;
    }

    public void setTypeOfObject(String typeOfObject) {
        this.typeOfObject = typeOfObject;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getParameter() {
        return parameter;
    }

    public void setParameter(int parameter) {
        this.parameter = parameter;
    }

    /**
     * Üres konstruktor
     */
    public Instruction() {

    }

    /**
     * Paraméterezett konstruktor
     * @param tileNum Az érintett csempe száma
     * @param typeOfObject Az érintett objektum típusa (Thing, Animal, Tile)
     * @param method A hívott metódus neve
     * @param parameter A hívott metódus paramétere
     */
    public Instruction(int tileNum, String typeOfObject, String method, int parameter) {
        this.tileNum = tileNum;
        this.typeOfObject = typeOfObject;
        this.method = method;
        this.parameter = parameter;
    }


    /**
     * Az eltárolt parancs végrehajtására szolgáló metódus
     */
    public void execute() {

        Tile affectedTile = Game.getInstance().getCurrentMap().getTile(this.tileNum - 1);

        // a parancs kisbetűsítése az egyszerűbb kezelhetőségért és a kis/nagybetűs írás tolerálásáért
        switch(this.typeOfObject.toLowerCase()) {
            case "animal":
                switch (this.method.toLowerCase()) {
                    case "moveto":
                        Tile destinationTile = Game.getInstance().getCurrentMap().getTile(this.parameter - 1);
                        affectedTile.getAnimal().moveTo(destinationTile);
                        break;
                    case "act":
                        affectedTile.getAnimal().act();
                        break;
                    default:
                        System.out.println("Animal-ön nem végrehajtható parancs");
                        break;
                }
                break;
            case "thing":
                switch (this.method.toLowerCase()) {
                    case "act":
                        affectedTile.getThing().act();
                        break;
                    default:
                        System.out.println("Thing-en nem végrehajtható parancs");
                        break;
                }
                break;
            case "tile":
                System.out.println("Tile-on nem hívható parancs ilyen módon");
                break;
            default:
                System.out.println("Híbás típus");
                break;
        }
    }
}
