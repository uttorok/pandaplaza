package com.uttorok.pandaplaza;

import java.util.Random;

public class SlotMachine extends Thing {
    public SlotMachine(){

    }

    /**
     * SlotMachine acts (jingles randomly)
     */
    @Override
    public void act(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": act()");

        Random r = new Random();
        int random = r.nextInt(100);
        if(random <= 30) {
            jingle();
        }

        Indentation.decreaseDepth();
    }

    /**
     * Slotmachine jingles
     */
    public void jingle(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": jingle()");

        tile.affectNeighbors(this);

        Indentation.decreaseDepth();
    }
}
