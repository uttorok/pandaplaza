package com.uttorok.pandaplaza;

import javax.swing.*;

/**
 * An abstract class that implements generic Animal behavior
 */
public abstract class Animal implements IAction{
    /**
     * The Tile on which the Animal currently is
     */
    protected Tile tile;

    /**
     * Contains the Animal in front of it, if any, otherwise null
     */
    protected Animal animalAtFront;

    /**
     * Contains the Animal behind it, if any, otherwise null
     */
    protected Animal animalBehind;

    public void setTile(Tile t) {
        tile = t;
    }

    public Tile getTile() {
        return tile;
    }

    public Animal getAnimalAtFront() { return this.animalAtFront; }

    public void setAnimalAtFront(Animal animalAtFront) {
        this.animalAtFront = animalAtFront;
    }

    public Animal getAnimalBehind() { return this.animalBehind; }

    public void setAnimalBehind(Animal animalBehind) {
        this.animalBehind = animalBehind;
    }

    /**
     * Makes free pandas move
     */
    @Override
    public void act() {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": act()");

        Indentation.decreaseDepth();
    }

    /**
     * 2
     *
     * @param t Tile where the Animal goes to
     */
    public void moveTo(Tile t){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + t.getClass().getSimpleName() + ")");

        tile = t;

        Indentation.decreaseDepth();
    }

    /**
     * Animal dies
     */
    public void die(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");
        this.tile=null;
        Indentation.decreaseDepth();
    }

    /**
     * @param a the Armchair this Animal reacts to
     */
    public void reactionTo(Armchair a) {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + a.getClass().getSimpleName() + ")");

        Indentation.decreaseDepth();
    }

    /**
     * @param c the Chocomat this Animal reacts to
     */
    public void reactionTo(Chocomat c) {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + c.getClass().getSimpleName() + ")");

        Indentation.decreaseDepth();
    }

    /**
     * @param s the SlotMachine this Animal reacts to
     */
    public void reactionTo(SlotMachine s) {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + s.getClass().getSimpleName() + ")");

        Indentation.decreaseDepth();
    }

    /**
     * @param w theWardrobe this Animal reacts to
     */
    public void reactionTo(Wardrobe w) {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + w.getClass().getSimpleName() + ")");

        Indentation.decreaseDepth();
    }

    /**
     * @param o the Orangutan this Animal reacts to
     */
    public void reactionTo(Orangutan o) {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + o.getClass().getSimpleName() + ")");

        Indentation.decreaseDepth();
    }

    /**
     * @param p the Panda this Animal reacts to
     */
    public void reactionTo(Panda p){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + p.getClass().getSimpleName() + ")");

        Indentation.decreaseDepth();
    }
    /**
     * Recursively pulls all Pandas to the next Tile
     *
     * @param t New Tile
     */
    public void pullTo(Tile t){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + t.getClass().getSimpleName() + ")");

        Tile oldTile = this.tile;

        t.accept(this, this.tile);

        if(this.animalBehind != null){
            this.animalBehind.pullTo(oldTile);
        }

        Indentation.decreaseDepth();
    }

    /**
     * Sets animalAtFront
     *
     * @param a Panda at front
     */
    public void setFirstPosition(Animal a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(" + a.getClass().getSimpleName() + ")");

        animalAtFront = a;

        Indentation.decreaseDepth();
    }

    /**
     * Recursively kills all Animals behind, and adds points
     */
    public void recursivelyKill(Orangutan o){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");

        Game.getInstance().addPoints(1, o);

        if(animalBehind != null){
            animalBehind.recursivelyKill(o);
        }

        die();

        Indentation.decreaseDepth();
    }

    /**
     * Recursively release all Pandas behind in the line
     * Releases animalAtFront and calls releaseBackwards
     */
    public void announceRelease(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");

        releaseForward();

        releaseBackwards();



        Indentation.decreaseDepth();
    }

    /**
     * If this has an animal in front of it, release that animal
     */
    public void releaseForward(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");

        if(animalAtFront != null)
            animalAtFront = null;

        Indentation.decreaseDepth();
    }

    /**
     * Puts Animal in line
     *
     * @param animalFront Sets animal that is in front of it in the line
     * @param animalBack Sets animal that is behind it in the line
     */
    public void chain(Animal animalFront, Animal animalBack){
        Indentation.increaseDepth();
        String printString = Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(";

        if(animalFront == null) {
            printString += "null, ";
        }
        else {
            printString += animalFront.getClass().getSimpleName() + ", ";
        }

        if(animalBack == null) {
            printString += "null)";
        }
        else {
            printString += animalBack.getClass().getSimpleName() + ")";
        }
        System.out.println(printString);
        animalAtFront = animalFront;
        animalBehind = animalBack;

        if(animalAtFront != null) {
            animalAtFront.setAnimalBehind(this);
        }
        if(animalBehind != null) {
            animalBehind.setAnimalAtFront(this);
        }

        if(animalBack != null) {
            animalBack.setFirstPosition(this);
        }

        Indentation.decreaseDepth();
    }

    /**
     * If this has an animal behind it, release that animal
     */
    public void releaseBackwards() {
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");


        if(animalBehind != null) {
            animalBehind.announceRelease();
            animalBehind = null;
        }

        Indentation.decreaseDepth();
    }

    /**
     * @param screenScale helps scale the graphics properly
     */
    abstract public void draw(double screenScale);


    // A következő függvények szerializálás miatt szükségesek az Animalbe

    public int getLife() { return 0; }

    public int getWaitTime() { return 0;}

    public int getEnergy() { return 0;}

    public Armchair getArmchair() {
        return null;
    }



}
