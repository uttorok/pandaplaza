package com.uttorok.pandaplaza;


import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Tile {
    private JButton b;

    private Thing thing;
    protected Animal animal;
    private ArrayList<Tile> neighbors;

    public Tile(){
        neighbors = new ArrayList<Tile>();
        b = new JButton("Step");
        b.addActionListener(new TileButtonActionListener(this));
    }

    /**
     * @param buttonX coordinate
     * @param buttonY coordinate
     * @param screenScale helps with graphics scaling
     */
    public void setButtonParameters(int buttonX, int buttonY, double screenScale){
        b.setBounds(buttonX, buttonY, (int) (80 * screenScale) , (int) (80 * screenScale));
    }

    public JButton getButton() {
        return b;
    }

    /**
     * @param img the Image to draw on the button
     */
    public void setButtonIMG(Image img) {
        if(img == null) b.setIcon(null);
        else b.setIcon(new ImageIcon(img));
    }

    public void addNeighbor(Tile t){
        neighbors.add(t);
    }

    public ArrayList<Tile> getNeighbors(){
        return neighbors;
    }

    public void addThing(Thing t){
        thing = t;
    }

    public void setAnimal(Animal a) {
        animal = a;
    }

    public Animal getAnimal() {
        if(animal == null) {

        }
        return animal;
    }

    public Thing getThing() {
        return thing;
    }

    public void setThing(Thing thing) {
        this.thing = thing;
    }

    public int weakLife(){return 0;}

    /**
     * Accept orangutan from another tile
     * @param animalComing Animal which is accept by the tile
     * @param oldTile The old tile
     */
    public void accept(Animal animalComing, Tile oldTile){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": accept(" + animalComing.getClass().getSimpleName() + ", " + oldTile.getClass().getSimpleName() + ")");
        if(this.getClass().getSimpleName().equals("Entrance") && oldTile.getClass().getSimpleName().equals("Exit") && animalComing.getClass().getSimpleName().equals("Orangutan")) {
            oldTile.remove(animalComing);
            this.take(animalComing);
            ((Orangutan)animalComing).killPandas();
        }
        else if(this.animal != null){
            // Orangután Orangutános csempére lép
            if(animalComing.getClass().getSimpleName().equals("Orangutan") && this.animal.getClass().getSimpleName().equals("Orangutan")){
                // eltároljuk a csempén lévő Orangutánt
                Orangutan temp = (Orangutan) this.animal;
                // a jövő Orangután átlép ide (a csempéje már ez lesz)
                this.take(animalComing);
                // a régicsempe étveszi a másik Orangutánt (a csempéje már az lesz)
                oldTile.take(temp);
                // a jövő Orangután reagál a másikra
                animalComing.reactionTo(temp);
            }
            // Orangután Pandás csempére lép
            else if(animalComing.getClass().getSimpleName().equals("Orangutan") && (this.animal.getClass().getSimpleName().equals("TiredPanda") || this.animal.getClass().getSimpleName().equals("TimidPanda") || this.animal.getClass().getSimpleName().equals("JumpyPanda"))){
                // eltároljuk a csempén lévő Pandát
                Panda temp = (Panda) this.animal;
                this.take(animalComing);
                oldTile.take(temp);
                animalComing.reactionTo(temp);
            }
            // bármi üres csempére lép
            else if(this.animal == null) {
                oldTile.remove(animalComing);
                this.take(animalComing);
            }
            // többi eset: Panda Pandás csempére lép, Panda Orangutános csempére lép... nem lehet.
            else {

            }
        }
        else {
            oldTile.remove(animalComing);
            this.take(animalComing);
        }
        Indentation.decreaseDepth();
    }

    //public void accept(Orangutan o, Exit e) {}

    /**
     * Remove animal from tile
     * @param a Animal which is removed
     */
    public void remove(Animal a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": remove(" + a.getClass().getSimpleName() + ")");
        if(animal == a){
            animal = null;
        }
        Indentation.decreaseDepth();
    }

    /**
     * Animal jumped on to the tile
     * @param a Animal which is jumped.
     */
    public void jumpedOntoBy(Animal a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": jumpedOntoBy(" + a.getClass().getSimpleName() + ")");
        Indentation.decreaseDepth();
    }

    /**
     * Tile affects neighbors to activate them
     * @param a Thing which is affected
     */
    public void affectNeighbors(Armchair a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": affectNeighbors(" + a.getClass().getSimpleName() + ")");

        for(Tile neighborTile : neighbors){
            if(neighborTile.animal != null) {
                neighborTile.animal.reactionTo(a);
            }
        }

        Indentation.decreaseDepth();
    }

    public void affectNeighbors(Chocomat c){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": affectNeighbors(" + c.getClass().getSimpleName() + ")");

        for(Tile neighborTile : neighbors){
            if(neighborTile.animal != null){
                neighborTile.animal.reactionTo(c);
            }
        }

        Indentation.decreaseDepth();
    }

    public void affectNeighbors(SlotMachine s){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": affectNeighbors(" + s.getClass().getSimpleName() + ")");

        for(Tile neighborTile : neighbors){
            if(neighborTile.animal != null){
                neighborTile.animal.reactionTo(s);
            }
        }

        Indentation.decreaseDepth();
    }

    /*public void affectNeighbors(Wardrobe w){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": affectNeighbors(" + w.getClass().getSimpleName() + ")");

        for(Tile neighborTile : neighbors){
            neighborTile.animal.reactionTo(w);
        }

        Indentation.decreaseDepth();
    }*/

    /**
     * Take animals from old tile
     * @param a Animal which is took.
     */
    public void take(Animal a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": take(" + a.getClass().getSimpleName() + ")");
        animal = a;
        a.tile = this;
        Indentation.decreaseDepth();
    }

    /**
     * The effect of the Wardrobe gets passed onto the animal on the tile
     * @param w the Wardrobe that affects the Tile
     */
    public void affect(Wardrobe w){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": affect(" + w.getClass().getSimpleName() + ")");

        if(animal != null) {
            animal.reactionTo(w);
        }

        Indentation.decreaseDepth();
    }

    /**
     * Move animal to neighbor tile,
     * @param a
     */
    public void moveToNeighborTile(Animal a){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": moveToNeighborTile(" + a.getClass().getSimpleName() + ")");

        // próbálunk egy szabad szomszédos csempét találni
        Random rand = new Random();
        // 20-szor próbálkozunk random szám generálással maximum
        int tries = 20;
        int i = 0;
        Tile randomNeighbor = neighbors.get(rand.nextInt(neighbors.size()));
        // amíg nem találunk olyat, ami üres, maximum "tries" próbálkozásig
        while((randomNeighbor.getAnimal() != null || randomNeighbor.getThing() != null) && i < tries) {
            randomNeighbor = neighbors.get(rand.nextInt(neighbors.size()));
            i++;
        }

        // ha 20-alatt találtunk üres csempét, akkor jó, de ha 20 lefutás után sem, akkor rossz csempe, ezért az ellenőrzés
        if(randomNeighbor.getAnimal() == null && randomNeighbor.getThing() == null){
            // az üres csempe acceptálja a pandát
            randomNeighbor.accept(a,this);
        }

        Indentation.decreaseDepth();
    }

    /**
     * Disables button
     */
    public void disableButton() {
        b.setEnabled(false);
    }

    /**
     * Enables button
     */
    public void enableButton() {
        b.setEnabled(true);
    }

    /**
     * @return Enables those neighboring buttons, that an Orangutan can step on
     */
    public int enableNeighborButtons() {
        int enabledButtons = 0;
        for (Tile t: neighbors) {
            // ha van rajta thing
            if(t.getThing() != null) {
                // a thing egy szekrény
                if(t.getThing().getClass().getSimpleName().equals("Wardrobe")) {
                    // és van rajta állat, akkor tiltjuk
                    if(t.getAnimal() != null) {
                        t.disableButton();
                    }
                    // nincs rajta állat, akkor engedélyezzük
                    else {
                        t.enableButton();
                        enabledButtons++;
                    }
                }
                // a thing nem szekrény, tiltjuk
                else {
                    t.disableButton();
                }
            }
            // nincs rajta thing
            else {
                // van rajta állat
                if(t.getAnimal() != null) {
                    // az állat sorba van fűzve, akkor tiltjuk
                    if(t.getAnimal().getAnimalAtFront() != null) {
                        t.disableButton();
                    }
                    // nincs sorba fűzve
                    else {
                        // Orangutánnak még várnia kell, mert nem régen vett el Pandákat
                        if(t.getAnimal().getClass().getSimpleName().equals("Orangutan") && this.animal.getWaitTime() > 0) {
                            t.disableButton();
                        }
                        else {
                            t.enableButton();
                            enabledButtons++;
                        }
                    }
                }
                // nincs rajta állat, engedélyezzük
                else {
                    t.enableButton();
                    enabledButtons++;
                }
            }
        }
        return enabledButtons;
    }
}
