package com.uttorok.pandaplaza;

import javax.swing.*;

/**
 * Class that implements behavior that is specific to Pandas that jump
 */
public class JumpyPanda extends Panda {
    public JumpyPanda(){

    }
    /**
     * Implements jumping behavior
     */
    private void jump(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": jump()");

        tile.jumpedOntoBy(this);

        Indentation.decreaseDepth();
    }

    /**
     * @param c Chocomat to react to
     */
    @Override
    public void reactionTo(Chocomat c){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": reactionTo(" + c.getClass().getSimpleName() + ")");

        this.jump();

        Indentation.decreaseDepth();
    }

    /**
     * @param screenScale helps scale the graphics properly 
     */
    @Override
    public void draw(double screenScale) {
        try {
            if (tile != null)
                tile.setButtonIMG(new ImageIcon("source\\images\\animals\\jumpypanda.png").getImage().getScaledInstance((int) (85 * screenScale), (int) (85 * screenScale),100));

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }


}
