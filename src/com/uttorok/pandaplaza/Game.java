package com.uttorok.pandaplaza;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Singleton Game class
 */
public class Game {
    /**
     * The only one Game object
     *
     * "One Game to rule them all, One Game to find them, One Game to bring them all, and in the darkness bind them"
     */
    private static Game ourInstance = new Game();

    /**
     * @return Returns the Game object
     */
    public static Game getInstance() {
        return ourInstance;
    }

    /**
     * Points of Player 1
     */
    private int pointsOfPlayer1;

    /**
     * Points of Player 2
     */
    private int pointsOfPlayer2;

    /**
     * Contains the actual Map
     */
    private Map currentMap;

    /**
     * Stores the two Orangutans
     */
    private ArrayList<Orangutan> orangutans;

    /**
     * The Orangutan that has its turn
     */
    private int activeOrangutan;

    private Game() {
        pointsOfPlayer1 = 0;
        pointsOfPlayer2 = 0;
        orangutans = new ArrayList<Orangutan>();
    }

    /**
     * @param o the Orangutan that gets added to the list
     */
    public void addOrangutan(Orangutan o) {
        orangutans.add(o);
    }

    public Map getCurrentMap() {
        return currentMap;
    }

    public void setCurrentMap(Map currentMap) {
        this.currentMap = currentMap;
    }

    public ArrayList<Orangutan> getOrangutans() {
        return orangutans;
    }

    /**
     * Adds points to player
     *
     * @param point Value of points
     */
    public void addPoints(int point, Orangutan o){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "(int)");

        if(o == orangutans.get(0)){
            pointsOfPlayer1 += point;
        } else if(o == orangutans.get(1)){
            pointsOfPlayer2 += point;
        } else {
            System.out.println("Baj van - Game: addPoints");
        }

        Indentation.decreaseDepth();
    }

    /**
     * Starts game
     */
    public void startGame(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");
        activeOrangutan = 0;
        Indentation.decreaseDepth();
    }

    /**
     * Starts the next turn: the active Orangutan's available tiles get enabled.
     */
    public void nextTurn() {
        currentMap.disableButtons();
        if(orangutans.size() == 0) {
            endGame();
        }
        else {
            int enabledButtons1 = getActiveOragutan().getTile().enableNeighborButtons();
            /*
            if(enabledButtons1 == 0){
                switchActiveOrangutan();
                currentMap.disableButtons();
                int enabledButtons2 = getActiveOragutan().getTile().enableNeighborButtons();
                if(enabledButtons2 == 0){
                    endGame();
                }
            }
            */
        }
        currentMap.reDraw();
    }

    /**
     * Ends game, and displays the winner
     */
    public void endGame(){
        Indentation.increaseDepth();
        System.out.println(Indentation.indent() + this.getClass().getSimpleName() + ": " + new Object(){}.getClass().getEnclosingMethod().getName() + "()");
        String winner;
        if(pointsOfPlayer1 == pointsOfPlayer2){
            winner = "Draw";
        }
        else if(pointsOfPlayer1 > pointsOfPlayer2){
            winner = "Player1";
        }
        else{
            winner = "Player2";
        }

        String str = "Points\n\n" +
                "Player 1: " + pointsOfPlayer1 + "\n" +
                "Player 2: " + pointsOfPlayer2 + "\n\n" +
                "Winner: " + winner;

        //image transform and resize
        JOptionPane.showMessageDialog(getCurrentMap().getFrame(), str,
                "Result", JOptionPane.INFORMATION_MESSAGE,
                null);

        System.exit(0);

        Indentation.decreaseDepth();
    }

    /**
     * @param i index for the list
     * @param o the Orangutan to set in the list
     */
    public void setOrangutan( int i, Orangutan o) {
        orangutans.set(i-1, o);
    }

    /**
     * @param o the Orangutan that should be removed from the list
     */
    public void removeOrangutan(Orangutan o) {
        orangutans.remove(o);
    }

    /**
     * @return the Orangutan that is currently on turn
     */
    public Orangutan getActiveOragutan() {
        return orangutans.get(activeOrangutan);
    }

    /**
     * @return the index of the currently active orangutan in the list
     */
    public int getActiveOrangutanIndex() { return activeOrangutan; }


    /**
     * Change the currently active orangutan's index
     */
    public void switchActiveOrangutan() {
        if(activeOrangutan == 0) {
            switch(orangutans.size()) {
                case 0:
                    endGame();
                    break;
                case 1:
                    activeOrangutan = 0;
                    break;
                case 2:
                    activeOrangutan = 1;
                    break;
                default:
                    endGame();
                    break;
            }
        }
        else {
            switch(orangutans.size()) {
                case 0:
                    endGame();
                    break;
                case 1:
                case 2:
                    activeOrangutan = 0;
                    break;
                default:
                    endGame();
                    break;
            }
        }
    }

    /**
     * @return the first player's points
     */
    public int getPointsOfPlayer1() {
        return pointsOfPlayer1;
    }

    /**
     * @return the second player's points
     */
    public int getPointsOfPlayer2() {
        return pointsOfPlayer2;
    }
}
