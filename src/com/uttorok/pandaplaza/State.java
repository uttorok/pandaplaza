package com.uttorok.pandaplaza;

import java.util.ArrayList;

/**
 * Állapot tárolására szolgáló "szerializáló" osztály
 */
public class State {

    /**
     * pálya mérete
     */
    int size;

    /**
     * szomszédsági mátrix
     */
    private boolean[][] matrix;

    /**
     * Csempék és rajtuk lévő dolgok információit tartalmazó lista
     */
    private ArrayList<TileInfo> tileInfoList;

    /**
     * Pálya méretét tartalmazó konstruktor
     * @param size pálya mérete
     */
    public State(int size) {
        this.size = size;
        tileInfoList = new ArrayList<TileInfo>();
        matrix = new boolean[size][size];
    }

    public void addTileInfoToArray(TileInfo ti) {
        tileInfoList.add(ti);
    }

    public void setMatrix(int x, int y, boolean z) {
        matrix[x][y] = z;
    }

    public ArrayList<TileInfo> getTileInfoList() {
        return tileInfoList;
    }

    public boolean[][] getMatrix() {
        return matrix;
    }

    public void setTileInfoList(ArrayList<TileInfo> tileInfoList) {
        this.tileInfoList = tileInfoList;
    }
}
